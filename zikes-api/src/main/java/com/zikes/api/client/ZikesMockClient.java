package com.zikes.api.client;

import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.zikes.api.config.ZikesSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;

public class ZikesMockClient implements ZikesClient {
    private static final Logger log = LoggerFactory.getLogger(ZikesMockClient.class);

    private final ZikesSettings settings;

    public ZikesMockClient(ZikesSettings settings) {
        this.settings = settings;
    }

    @Override
    public String fetch(String resource, HTTPRequest request) {
        try {
            URL url = Resources.getResource("dev/" + resource + ".json");

            return Resources.toString(url, Charsets.UTF_8);
        } catch (Exception e) {
            log.warn(String.format("Failed to read mock resource ::: %s", resource), e);
        }

        return null;
    }

}
