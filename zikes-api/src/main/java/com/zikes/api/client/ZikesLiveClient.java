package com.zikes.api.client;

import com.google.appengine.api.urlfetch.*;
import com.google.common.base.MoreObjects;
import com.zikes.api.config.ZikesSettings;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.io.IOException;

public class ZikesLiveClient implements ZikesClient {
    private static final Logger log = LoggerFactory.getLogger(ZikesLiveClient.class);
    private static final Double MAX_TIMEOUT = 10.0; //in seconds

    private final ZikesSettings settings;

    public ZikesLiveClient(ZikesSettings settings) {
        this.settings = settings;
    }

    @Override
    public String fetch(String resource, HTTPRequest request) throws ZikesClientException {
        URLFetchService service = URLFetchServiceFactory.getURLFetchService();

        try {
            request.addHeader(new HTTPHeader("Authorization", "Basic " + Base64.encodeBase64String((settings.getUsername() + ":" + settings.getPassword()).getBytes())));
            request.getFetchOptions().setDeadline(MAX_TIMEOUT);

            HTTPResponse response = service.fetch(request);
            String responseBody = new String(response.getContent(), "UTF-8");

            if (HttpStatus.OK.value() == response.getResponseCode()) {
                return responseBody;
            } else {
                throw new ZikesClientException(HttpStatus.valueOf(response.getResponseCode()), MoreObjects.firstNonNull(responseBody, "Zikes server failed to process request"));
            }
        } catch (IOException e) {
            log.warn(String.format("Failed to fetch data from zikes server ::: %s", request.getURL()), e);
            throw new ZikesClientException(HttpStatus.BAD_GATEWAY, "Failed to fetch data from zikes server");
        }
    }
}
