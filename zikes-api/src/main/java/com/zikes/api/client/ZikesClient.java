package com.zikes.api.client;

import com.google.appengine.api.urlfetch.HTTPRequest;

public interface ZikesClient {
    public String fetch(String resource, HTTPRequest request) throws ZikesClientException;
}
