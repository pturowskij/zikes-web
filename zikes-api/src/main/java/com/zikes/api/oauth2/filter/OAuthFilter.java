package com.zikes.api.oauth2.filter;

import com.google.common.base.Enums;
import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.zikes.api.model.ZikesUser;
import com.zikes.api.oauth2.OAuthAccessToken;
import com.zikes.api.oauth2.OAuthAuthentication;
import com.zikes.api.oauth2.OAuthHeaders;
import com.zikes.api.oauth2.OAuthProvider;
import com.zikes.api.storage.ds.UserLookupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.BearerTokenExtractor;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Validate request headers contain oauth provider and access token.
 * If that's the case check token is not expired and update spring security context
 * Later in code <code>org.springframework.security.access.annotation.Secured</code> annotation
 * can be used to restrict endpoint/method access
 */
public class OAuthFilter extends OncePerRequestFilter {
    @Autowired
    private UserLookupService userLookupService;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        String authorizationProvider = request.getHeader(OAuthHeaders.OAUTH_PROVIDER);
        Authentication authentication = new BearerTokenExtractor().extract(request);

        if (!Strings.isNullOrEmpty(authorizationProvider) && null != authentication) {
            Optional<OAuthProvider> knownProvider = Enums.getIfPresent(OAuthProvider.class, authorizationProvider.toUpperCase());

            if (knownProvider.isPresent()) {
                Optional<OAuthAccessToken> validatedToken = knownProvider.get().getValidator().getIfValid((String) authentication.getPrincipal());

                if (validatedToken.isPresent()) {
                    ZikesUser zikesUser = userLookupService.getOrCreate(validatedToken.get());

                    SecurityContextHolder.getContext().setAuthentication(new OAuthAuthentication.Builder()
                            .authorities(AuthorityUtils.createAuthorityList("ROLE_USER"))
                            .authenticated(true)
                            .accessToken(validatedToken.get())
                            .zikesUser(zikesUser)
                            .build());

                }
            }
        }

        filterChain.doFilter(request, response);
    }
}
