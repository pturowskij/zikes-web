package com.zikes.api.oauth2;

import com.google.common.base.Optional;
import com.zikes.api.exceptions.ServiceException;

public interface OAuthTokenValidatorIfc {
    Optional<OAuthAccessToken> getIfValid(String accessToken) throws ServiceException;
}
