package com.zikes.api.oauth2;

import com.zikes.api.oauth2.validator.FacebookTokenValidator;
import com.zikes.api.oauth2.validator.GoogleTokenValidator;

public enum OAuthProvider {
    GOOGLE(new GoogleTokenValidator()), FACEBOOK(new FacebookTokenValidator());

    private final OAuthTokenValidatorIfc validator;

    OAuthProvider(OAuthTokenValidatorIfc validator) {
        this.validator = validator;
    }

    public OAuthTokenValidatorIfc getValidator() {
        return validator;
    }
}
