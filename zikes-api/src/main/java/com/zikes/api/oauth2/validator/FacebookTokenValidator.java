package com.zikes.api.oauth2.validator;

import com.fasterxml.jackson.databind.JsonNode;
import com.zikes.api.oauth2.OAuthAccessToken;
import com.zikes.api.oauth2.OAuthProvider;

import java.net.MalformedURLException;
import java.net.URL;

public class FacebookTokenValidator extends AbstractTokenValidator {
    private static final String TOKEN_INFO_ENDPOINT = "https://graph.facebook.com/me?fields=id&access_token=%s";

    @Override
    protected URL getEndpoint(String accessToken) throws MalformedURLException {
        return new URL(String.format(TOKEN_INFO_ENDPOINT, accessToken));
    }

    @Override
    protected OAuthAccessToken parseToken(JsonNode json) {
        OAuthAccessToken token = new OAuthAccessToken();
        token.setProvider(OAuthProvider.FACEBOOK);
        token.setUserId(json.get("id").asText());

        return token;
    }
}
