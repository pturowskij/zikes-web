package com.zikes.api.oauth2;

public class OAuthAccessToken {
    private OAuthProvider provider;
    private String userId;
    private long expiresIn;

    public OAuthAccessToken() {
    }

    public OAuthAccessToken(OAuthProvider provider, String userId) {
        this.provider = provider;
        this.userId = userId;
    }

    public OAuthProvider getProvider() {
        return provider;
    }

    public void setProvider(OAuthProvider provider) {
        this.provider = provider;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(long expiresIn) {
        this.expiresIn = expiresIn;
    }
}
