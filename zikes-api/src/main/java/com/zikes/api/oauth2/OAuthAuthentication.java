package com.zikes.api.oauth2;

import com.zikes.api.model.ZikesUser;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

public class OAuthAuthentication extends AbstractAuthenticationToken {
    private final OAuthAccessToken accessToken;
    private final ZikesUser zikesUser;

    private OAuthAuthentication(Builder builder) {
        super(builder.authorities);
        setAuthenticated(builder.authenticated);

        this.accessToken = builder.accessToken;
        this.zikesUser = builder.zikesUser;
    }

    public OAuthAccessToken getAccessToken() {
        return accessToken;
    }

    public ZikesUser getZikesUser() {
        return zikesUser;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }

    public static final class Builder {
        private List<GrantedAuthority> authorities;
        private boolean authenticated;
        private OAuthAccessToken accessToken;
        private ZikesUser zikesUser;

        public Builder() {
        }

        public Builder authenticated(boolean authenticated) {
            this.authenticated = authenticated;
            return this;
        }

        public Builder authorities(List<GrantedAuthority> authorities) {
            this.authorities = authorities;
            return this;
        }

        public Builder accessToken(OAuthAccessToken accessToken) {
            this.accessToken = accessToken;
            return this;
        }

        public Builder zikesUser(ZikesUser zikesUser) {
            this.zikesUser = zikesUser;
            return this;
        }

        public OAuthAuthentication build() {
            return new OAuthAuthentication(this);
        }
    }
}
