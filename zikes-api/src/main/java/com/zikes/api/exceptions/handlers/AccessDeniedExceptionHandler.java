package com.zikes.api.exceptions.handlers;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AccessDeniedExceptionHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);

        JsonNodeFactory nodeFactory = JsonNodeFactory.instance;
        ObjectNode exception = nodeFactory.objectNode();
        exception.put("title", HttpStatus.FORBIDDEN.getReasonPhrase());
        exception.put("status", HttpStatus.FORBIDDEN.value());
        exception.put("message", accessDeniedException.getMessage());

        response.getOutputStream().println(exception.toString());

    }
}
