package com.zikes.api.config.profiles;

import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class LiveProfile implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        return FluentIterable.of(context.getEnvironment().getActiveProfiles()).anyMatch(
                new Predicate<String>() {
                    @Override
                    public boolean apply(String profile) {
                        return profile.contains("dev") || profile.contains("prod");
                    }
                }
        );
    }
}