package com.zikes.api.endpoints.proxy;

import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.zikes.api.config.ZikesSettings;
import com.zikes.api.exceptions.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.concurrent.Callable;

@RestController
@RequestMapping("/route")
public class RoutesEndpointBase extends ZikesServerProxyEndpointBase {

    @Autowired
    private ZikesSettings settings;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity route(final @RequestBody String body, final HttpServletRequest request) throws ServiceException {
        return proxyZikesRequest("route", new Callable<HTTPRequest>() {
            @Override
            public HTTPRequest call() throws Exception {
                URL url = new URL(settings.getUrl() + "/route?" + request.getQueryString());
                HTTPRequest proxyRequest = new HTTPRequest(url, HTTPMethod.POST);
                proxyRequest.addHeader(new HTTPHeader("Content-Type", "application/json; charset=utf-8"));
                proxyRequest.setPayload(body.getBytes(Charset.forName("UTF-8")));

                return proxyRequest;
            }
        });
    }
}
