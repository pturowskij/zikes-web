package com.zikes.api.endpoints;

import com.google.common.base.Preconditions;
import com.zikes.api.exceptions.GcsAclAccessException;
import com.zikes.api.exceptions.ServiceException;
import com.zikes.api.model.Journey;
import com.zikes.api.model.Metadata;
import com.zikes.api.oauth2.OAuthAuthentication;
import com.zikes.api.storage.AclChecker;
import com.zikes.api.storage.ZikesIdManager;
import com.zikes.api.storage.gcs.AccessControlledResourceService;
import org.joda.time.Instant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/users/{zikes_id}/plan/journeys")
public class JourneysEndpoint {

    @Autowired
    private AccessControlledResourceService<Journey> journeyService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({"ROLE_USER"})
    public List<Metadata> list(@PathVariable("zikes_id") String zikesEncodedId) throws ServiceException, IOException {
        OAuthAuthentication oauth = (OAuthAuthentication) SecurityContextHolder.getContext().getAuthentication();
        Preconditions.checkState(zikesEncodedId.equals(oauth.getZikesUser().getEncodedId()), "You can only list own journeys");

        return journeyService.list(oauth.getZikesUser().getZikesId());
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @Secured({"ROLE_USER"})
    public Metadata create(@Valid @RequestBody Journey journey) throws IOException {
        OAuthAuthentication oauth = (OAuthAuthentication) SecurityContextHolder.getContext().getAuthentication();

        Metadata metadata = journey.getMetadata();

        metadata.setId(ZikesIdManager.generateId());
        metadata.setOwner(oauth.getZikesUser().getZikesId());
        metadata.setCreated(Instant.now().getMillis());
        metadata.setLastModified(metadata.getCreated());

        journeyService.put(journey);

        return journey.getMetadata();
    }

    @RequestMapping(value = "/{journey_id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Journey get(@PathVariable("zikes_id") String zikesEncodedId, @PathVariable("journey_id") String journeyId) throws IOException {
        OAuthAuthentication oauth = (OAuthAuthentication) SecurityContextHolder.getContext().getAuthentication();

        Journey journey = journeyService.get(ZikesIdManager.decodeId(zikesEncodedId), journeyId);

        if (!AclChecker.hasReadAccess(oauth, journey.getMetadata()))
            throw new GcsAclAccessException(String.format("No permission to access journey : [%s]", journeyId));

        return journey;
    }

    @RequestMapping(value = "/{journey_id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Secured({"ROLE_USER"})
    public Metadata update(@PathVariable("zikes_id") String zikesEncodedId, @PathVariable("journey_id") String journeyId, @Valid @RequestBody Journey journey) throws IOException {
        OAuthAuthentication oauth = (OAuthAuthentication) SecurityContextHolder.getContext().getAuthentication();

        Journey persistedJourney = journeyService.get(ZikesIdManager.decodeId(zikesEncodedId), journeyId);

        if (!AclChecker.hasWriteAccess(oauth, persistedJourney.getMetadata()))
            throw new GcsAclAccessException(String.format("No permission to modify journey : [%s]", journeyId));

        Metadata metadata = journey.getMetadata();

        metadata.setId(persistedJourney.getMetadata().getId());
        metadata.setOwner(persistedJourney.getMetadata().getOwner());
        metadata.setCreated(persistedJourney.getMetadata().getCreated());
        metadata.setLastModified(Instant.now().getMillis());

        journeyService.put(journey);

        return journey.getMetadata();
    }

    @RequestMapping(value = "/{journey_id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Secured({"ROLE_USER"})
    public void delete(@PathVariable("zikes_id") String zikesEncodedId, @PathVariable("journey_id") String journeyId) throws IOException {
        OAuthAuthentication oauth = (OAuthAuthentication) SecurityContextHolder.getContext().getAuthentication();

        Journey persistedJourney = journeyService.get(ZikesIdManager.decodeId(zikesEncodedId), journeyId);

        if (!AclChecker.hasWriteAccess(oauth, persistedJourney.getMetadata()))
            throw new GcsAclAccessException(String.format("No permission to delete journey : [%s]", journeyId));

        journeyService.delete(persistedJourney);
    }
}
