package com.zikes.api.endpoints.proxy;

import com.google.appengine.api.urlfetch.HTTPRequest;
import com.zikes.api.client.ZikesClient;
import com.zikes.api.client.ZikesClientException;
import com.zikes.api.config.ZikesSettings;
import com.zikes.api.exceptions.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.util.concurrent.Callable;

public abstract class ZikesServerProxyEndpointBase {

    @Autowired
    private ZikesClient zikesClient;

    @Autowired
    private ZikesSettings settings;

    @RequestMapping(method = RequestMethod.OPTIONS, value = "/**")
    public ResponseEntity corsEnabled() {
        return ResponseEntity.ok().build();
    }

    protected ResponseEntity proxyZikesRequest(String resource, Callable<HTTPRequest> prepare) throws ServiceException {
        try {
            HTTPRequest proxyRequest = prepare.call();

            String response = zikesClient.fetch(resource, proxyRequest);

            return new ResponseEntity(response, HttpStatus.OK);
        } catch (ZikesClientException e) {
            throw new ServiceException(e.getStatus(), e.getMessage());
        } catch (IOException ex) {
            throw new ServiceException("Server is misconfigured");
        } catch (Exception ex) {
            throw new ServiceException("Server failed with unexpected error");
        }
    }
}
