package com.zikes.api.endpoints;

import com.google.common.base.Preconditions;
import com.zikes.api.exceptions.GcsAclAccessException;
import com.zikes.api.exceptions.ServiceException;
import com.zikes.api.model.Metadata;
import com.zikes.api.model.Preferences;
import com.zikes.api.oauth2.OAuthAuthentication;
import com.zikes.api.storage.AclChecker;
import com.zikes.api.storage.ZikesIdManager;
import com.zikes.api.storage.gcs.AccessControlledResourceService;
import org.joda.time.Instant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/users/{zikes_id}/plan/preferences")
public class PreferencesEndpoint {
    @Autowired
    private AccessControlledResourceService<Preferences> preferencesService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({"ROLE_USER"})
    public List<Metadata> list(@PathVariable("zikes_id") String zikesEncodedId) throws ServiceException, IOException {
        OAuthAuthentication oauth = (OAuthAuthentication) SecurityContextHolder.getContext().getAuthentication();
        Preconditions.checkState(zikesEncodedId.equals(oauth.getZikesUser().getEncodedId()), "You can only list own preferences");

        return preferencesService.list(oauth.getZikesUser().getZikesId());
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @Secured({"ROLE_USER"})
    public Metadata create(@Valid @RequestBody Preferences preferences) throws IOException {
        OAuthAuthentication oauth = (OAuthAuthentication) SecurityContextHolder.getContext().getAuthentication();

        Metadata metadata = preferences.getMetadata();

        metadata.setId(ZikesIdManager.generateId());
        metadata.setOwner(oauth.getZikesUser().getZikesId());
        metadata.setCreated(Instant.now().getMillis());
        metadata.setLastModified(metadata.getCreated());

        preferencesService.put(preferences);

        return preferences.getMetadata();
    }

    @RequestMapping(value = "/{preferences_id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Preferences get(@PathVariable("zikes_id") String zikesEncodedId, @PathVariable("preferences_id") String preferencesId) throws IOException {
        OAuthAuthentication oauth = (OAuthAuthentication) SecurityContextHolder.getContext().getAuthentication();

        Preferences preferences = preferencesService.get(ZikesIdManager.decodeId(zikesEncodedId), preferencesId);

        if (!AclChecker.hasReadAccess(oauth, preferences.getMetadata()))
            throw new GcsAclAccessException(String.format("No permission to access preferencesId : [%s]", preferencesId));

        return preferences;
    }

    @RequestMapping(value = "/{preferences_id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Secured({"ROLE_USER"})
    public Metadata update(@PathVariable("zikes_id") String zikesEncodedId, @PathVariable("preferences_id") String preferencesId, @Valid @RequestBody Preferences preferences) throws IOException {
        OAuthAuthentication oauth = (OAuthAuthentication) SecurityContextHolder.getContext().getAuthentication();

        Preferences persistedPreferences = preferencesService.get(ZikesIdManager.decodeId(zikesEncodedId), preferencesId);

        if (!AclChecker.hasWriteAccess(oauth, persistedPreferences.getMetadata()))
            throw new GcsAclAccessException(String.format("No permission to modify preferencesId : [%s]", preferencesId));

        Metadata metadata = preferences.getMetadata();

        metadata.setId(persistedPreferences.getMetadata().getId());
        metadata.setOwner(persistedPreferences.getMetadata().getOwner());
        metadata.setCreated(persistedPreferences.getMetadata().getCreated());
        metadata.setLastModified(Instant.now().getMillis());

        preferencesService.put(preferences);

        return preferences.getMetadata();
    }

    @RequestMapping(value = "/{preferences_id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Secured({"ROLE_USER"})
    public void delete(@PathVariable("zikes_id") String zikesEncodedId, @PathVariable("preferences_id") String preferencesId) throws IOException {
        OAuthAuthentication oauth = (OAuthAuthentication) SecurityContextHolder.getContext().getAuthentication();

        Preferences persistedPreferences = preferencesService.get(ZikesIdManager.decodeId(zikesEncodedId), preferencesId);

        if (!AclChecker.hasWriteAccess(oauth, persistedPreferences.getMetadata()))
            throw new GcsAclAccessException(String.format("No permission to delete preferencesId : [%s]", preferencesId));

        preferencesService.delete(persistedPreferences);
    }
}
