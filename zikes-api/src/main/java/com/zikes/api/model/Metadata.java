package com.zikes.api.model;

import com.fasterxml.jackson.databind.node.ObjectNode;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class Metadata {
    private String id;
    private Long created;
    private Long lastModified;

    private Long owner;

    @NotNull
    @Valid
    private AccessLevel access;

    private ObjectNode opaque;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public Long getLastModified() {
        return lastModified;
    }

    public void setLastModified(Long lastModified) {
        this.lastModified = lastModified;
    }

    public Long getOwner() {
        return owner;
    }

    public void setOwner(Long owner) {
        this.owner = owner;
    }

    public AccessLevel getAccess() {
        return access;
    }

    public void setAccess(AccessLevel access) {
        this.access = access;
    }

    public ObjectNode getOpaque() {
        return opaque;
    }

    public void setOpaque(ObjectNode opaque) {
        this.opaque = opaque;
    }
}
