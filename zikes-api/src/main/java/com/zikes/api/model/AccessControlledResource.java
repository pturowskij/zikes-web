package com.zikes.api.model;

import com.fasterxml.jackson.databind.node.ObjectNode;

public interface AccessControlledResource {
    Metadata getMetadata();
    ObjectNode getContent();
}
