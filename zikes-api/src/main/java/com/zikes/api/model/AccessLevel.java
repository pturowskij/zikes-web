package com.zikes.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

public class AccessLevel {
    @NotNull
    private String ro;

    @NotNull
    private String rw;

    public AccessLevel() {
    }

    public AccessLevel(String ro, String rw) {
        this.ro = ro;
        this.rw = rw;
    }

    @JsonIgnore
    public boolean isReadPublic() {
        return "public".equalsIgnoreCase(ro);
    }

    @JsonIgnore
    public boolean isWritePublic() {
        return "public".equalsIgnoreCase(ro);
    }

    public List<String> readAccessUsers() {
        return Arrays.asList(ro.split(","));
    }

    public List<String> writeAccessUsers() {
        return Arrays.asList(rw.split(","));
    }

    public String getRo() {
        return ro;
    }

    public void setRo(String ro) {
        this.ro = ro;
    }

    public String getRw() {
        return rw;
    }

    public void setRw(String rw) {
        this.rw = rw;
    }
}
