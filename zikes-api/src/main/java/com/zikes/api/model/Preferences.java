package com.zikes.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class Preferences implements AccessControlledResource {
    @NotNull
    @Valid
    private Metadata metadata;

    @JsonProperty("preferences")
    @NotNull
    private ObjectNode content;

    public Preferences() {
    }

    public Preferences(Metadata metadata, ObjectNode content) {
        this.metadata = metadata;
        this.content = content;
    }

    @Override
    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    @Override
    public ObjectNode getContent() {
        return content;
    }

    public void setContent(ObjectNode content) {
        this.content = content;
    }
}
