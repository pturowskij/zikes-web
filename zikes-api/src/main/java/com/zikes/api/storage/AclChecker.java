package com.zikes.api.storage;

import com.zikes.api.model.Metadata;
import com.zikes.api.oauth2.OAuthAuthentication;

import javax.annotation.Nullable;

public class AclChecker {
    public static boolean isOwner(OAuthAuthentication oauth, Metadata metadata) {
        return metadata.getOwner().equals(oauth.getZikesUser().getZikesId());
    }

    public static boolean hasReadAccess(@Nullable OAuthAuthentication oauth, Metadata metadata) {
        if (metadata.getAccess().isReadPublic()) return true;
        if (null == oauth || !oauth.isAuthenticated()) return false;

        return isOwner(oauth, metadata) || metadata.getAccess().readAccessUsers().contains(oauth.getZikesUser().getEncodedId());
    }

    public static boolean hasWriteAccess(@Nullable OAuthAuthentication oauth, Metadata metadata) {
        if (metadata.getAccess().isWritePublic()) return true;
        if (null == oauth || !oauth.isAuthenticated()) return false;

        return isOwner(oauth, metadata) || metadata.getAccess().writeAccessUsers().contains(oauth.getZikesUser().getEncodedId());
    }
}
