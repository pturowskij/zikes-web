package com.zikes.api.storage.gcs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.util.Lists;
import com.google.appengine.tools.cloudstorage.*;
import com.zikes.api.config.GcsSettings;
import com.zikes.api.model.AccessControlledResource;
import com.zikes.api.model.Metadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.model.NotFoundException;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.util.Iterator;
import java.util.List;

public class AccessControlledResourceService<T extends AccessControlledResource> {
    public static String META_KEY = "meta";

    @Autowired
    protected GcsSettings settings;

    @Autowired
    protected GcsService gcsService;

    private final ResourceLocatorIfc resourceLocator;
    private final ResourceReaderIfc<T> resourceReader;

    public AccessControlledResourceService(ResourceLocatorIfc resourceLocator, ResourceReaderIfc<T> resourceReader) {
        this.resourceLocator = resourceLocator;
        this.resourceReader = resourceReader;
    }

    public List<Metadata> list(Long zikesId) throws IOException {
        ListResult list = gcsService.list(settings.getBucket(),
                new ListOptions.Builder()
                        .setPrefix(resourceLocator.getPath(zikesId))
                        .setRecursive(true)
                        .build());

        List<Metadata> metadataList = Lists.newArrayList();
        ObjectMapper objectMapper = new ObjectMapper();

        while (list.hasNext()) {
            ListItem item = list.next();
            if (!item.isDirectory()) {
                GcsFileMetadata fileMetadata = gcsService.getMetadata(new GcsFilename(settings.getBucket(), item.getName()));

                metadataList.add(objectMapper.readValue(fileMetadata.getOptions().getUserMetadata().get(META_KEY), Metadata.class));
            }
        }

        return metadataList;
    }

    public void putAll(Iterable<T> resources) throws IOException {
        Iterator<T> it = resources.iterator();

        while (it.hasNext()) {
            put(it.next());
        }
    }

    public void put(T resource) throws IOException {
        GcsFilename filename = new GcsFilename(settings.getBucket(), resourceLocator.getPath(resource.getMetadata()));
        ObjectMapper objectMapper = new ObjectMapper();

        GcsOutputChannel outputChannel =
                gcsService.createOrReplace(filename, new GcsFileOptions.Builder()
                        .mimeType("application/json")
                        .acl("bucket-owner-full-control")
                        .addUserMetadata(META_KEY, objectMapper.writeValueAsString(resource.getMetadata()))
                        .build());


        outputChannel.write(ByteBuffer.wrap(resource.getContent().toString().getBytes("UTF-8")));

        outputChannel.close();
    }

    public T get(Long zikesId, String resourceId) throws IOException {
        GcsFilename filename = new GcsFilename(settings.getBucket(), resourceLocator.getPath(zikesId, resourceId));

        GcsFileMetadata fileMetadata = gcsService.getMetadata(filename);

        if (null == fileMetadata) throw new NotFoundException(String.format("File not found : [%s]", filename));

        GcsInputChannel readChannel = gcsService.openReadChannel(filename, 0);

        InputStream is = Channels.newInputStream(readChannel);

        return resourceReader.read(fileMetadata, is);

    }

    public void delete(T resource) throws IOException {
        delete(resource.getMetadata());
    }

    public void delete(Metadata metadata) throws IOException {
        GcsFilename filename = new GcsFilename(settings.getBucket(), resourceLocator.getPath(metadata));

        gcsService.delete(filename);
    }
}
