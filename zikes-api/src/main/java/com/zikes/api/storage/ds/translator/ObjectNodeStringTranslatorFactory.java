package com.zikes.api.storage.ds.translator;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.googlecode.objectify.impl.Path;
import com.googlecode.objectify.impl.Property;
import com.googlecode.objectify.impl.translate.*;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ObjectNodeStringTranslatorFactory extends ValueTranslatorFactory<ObjectNode, String> {
    private static final Logger logger = Logger.getLogger(ObjectNodeStringTranslatorFactory.class.getName());

    private final ObjectMapper objectMapper = new ObjectMapper();

    public ObjectNodeStringTranslatorFactory() {
        super(ObjectNode.class);
    }

    @Override
    protected ValueTranslator<ObjectNode, String> createSafe(Path path, Property property, Type type, CreateContext ctx) {
        return new ValueTranslator<ObjectNode, String>(path, String.class) {

            @Override
            protected ObjectNode loadValue(String value, LoadContext ctx) throws SkipException {
                try {
                    return objectMapper.readValue(value, ObjectNode.class);
                } catch (IOException e) {
                    logger.log(Level.WARNING, "DS read failed", e);
                    throw new SkipException();
                }
            }

            @Override
            protected String saveValue(ObjectNode value, SaveContext ctx) throws SkipException {
                return value.toString();
            }
        };
    }
}
