package com.zikes.api.storage.gcs.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.appengine.tools.cloudstorage.GcsFileMetadata;
import com.zikes.api.model.Metadata;
import com.zikes.api.model.Preferences;
import com.zikes.api.storage.gcs.AccessControlledResourceService;
import com.zikes.api.storage.gcs.ResourceReaderIfc;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

public class PreferencesReaderImpl implements ResourceReaderIfc<Preferences> {
    @Override
    public Preferences read(GcsFileMetadata fileMetadata, InputStream is) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();

        Metadata metadata = objectMapper.readValue(fileMetadata.getOptions().getUserMetadata().get(AccessControlledResourceService.META_KEY), Metadata.class);
        ObjectNode journey = objectMapper.readValue(IOUtils.toString(is, Charset.forName("UTF-8")), ObjectNode.class);

        return new Preferences(metadata, journey);
    }
}

