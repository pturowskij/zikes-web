package com.zikes.api;

import com.google.common.base.MoreObjects;
import com.zikes.api.config.profiles.LiveProfile;
import com.zikes.api.config.profiles.MockProfile;
import com.zikes.api.exceptions.handlers.AccessDeniedExceptionHandler;
import com.zikes.api.oauth2.OAuthAccessToken;
import com.zikes.api.oauth2.OAuthAuthentication;
import com.zikes.api.oauth2.OAuthHeaders;
import com.zikes.api.oauth2.OAuthProvider;
import com.zikes.api.oauth2.filter.OAuthFilter;
import com.zikes.api.storage.ds.UserLookupService;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
@EnableWebMvcSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SecurityConfig {
    @Bean
    @Conditional(LiveProfile.class)
    WebSecurityConfigurerAdapter liveAuth() {
        return new WebSecurityConfigurerAdapter() {
            @Override
            protected void configure(HttpSecurity http) throws Exception {
                withDefaults(http).addFilterBefore(oAuthFilter(), BasicAuthenticationFilter.class);
            }
        };
    }

    @Bean
    @Conditional(LiveProfile.class)
    Filter oAuthFilter() {
        return new OAuthFilter();
    }

    @Bean
    @Conditional(MockProfile.class)
    WebSecurityConfigurerAdapter mockAuth() {
        return new WebSecurityConfigurerAdapter() {
            @Override
            protected void configure(HttpSecurity http) throws Exception {
                withDefaults(http).addFilterBefore(new OncePerRequestFilter() {
                    @Override
                    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
                        //let user provide mock configuration in request headers
                        OAuthAccessToken accessToken = new OAuthAccessToken(OAuthProvider.valueOf(MoreObjects.firstNonNull(request.getHeader(OAuthHeaders.OAUTH_PROVIDER.toUpperCase()), OAuthProvider.GOOGLE.name())),
                                MoreObjects.firstNonNull(request.getHeader(OAuthHeaders.OAUTH_MOCK_USERID), "1"));

                        SecurityContextHolder.getContext().setAuthentication(new OAuthAuthentication.Builder()
                                .authorities(AuthorityUtils.createAuthorityList("ROLE_USER"))
                                .authenticated(true)
                                .accessToken(accessToken)
                                .zikesUser(new UserLookupService().getOrCreate(accessToken))
                                .build());
                        filterChain.doFilter(request, response);
                    }
                }, BasicAuthenticationFilter.class);
            }
        };
    }


    private HttpSecurity withDefaults(HttpSecurity http) throws Exception {
        return http
                .csrf().disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests().anyRequest().permitAll()
                .and()
                .exceptionHandling().accessDeniedHandler(new AccessDeniedExceptionHandler())
                .and();
    }
}
