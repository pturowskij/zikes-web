package com.zikes.api.endpoints;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.internal.mapper.ObjectMapperType;
import com.zikes.api.model.AccessLevel;
import com.zikes.api.model.Journey;
import com.zikes.api.model.Metadata;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.when;
import static org.hamcrest.Matchers.hasItems;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JourneysEndpointTest extends AbstractFunctionalTest {
    Journey journey;

    @Before
    public void setUp() {
        super.setUp();

        Metadata metadata = new Metadata();
        metadata.setAccess(new AccessLevel("public", "public"));
        metadata.setOpaque(JsonNodeFactory.instance.objectNode().put("name", "journeyA"));
        journey = new Journey(metadata, JsonNodeFactory.instance.objectNode());
    }

    @Test
    public void stage1_canPost() {
        given()
                .contentType(ContentType.JSON)
                .body(journey, ObjectMapperType.JACKSON_2)
                .when()
                .post("/users/{zikes_id}/plan/journeys", zikesUser.getEncodedId())
                .then()
                .statusCode(HttpStatus.SC_CREATED);
    }

    @Test
    public void stage2_canList() {
        when()
                .get("/users/{zikes_id}/plan/journeys", zikesUser.getEncodedId())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("$", hasItems());
    }

}
