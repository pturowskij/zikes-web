package com.zikes.api.serialization;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zikes.api.model.Journey;
import org.junit.Assert;
import org.junit.Test;

import javax.validation.Validation;
import javax.validation.Validator;
import java.io.IOException;

public class JourneySerializationTest {
    ObjectMapper objectMapper = new ObjectMapper();
    Validator validator = Validation.byDefaultProvider().configure().buildValidatorFactory().getValidator();

    @Test
    public void deserializeValidJourney() throws IOException {
        String json = "{\"metadata\":{\"access\":{\"ro\":\"public\",\"rw\":\"\"},\"opaque\":{\"name\":\"route66\"}},\"journey\":{\"route#\":\"66\",\"coordinates\":[12.22,12.00]}}";

        Journey journey = objectMapper.readValue(json, Journey.class);

        Assert.assertTrue("public".equals(journey.getMetadata().getAccess().getRo()));
        Assert.assertTrue("route66".equals(journey.getMetadata().getOpaque().get("name").asText()));
        Assert.assertTrue("66".equals(journey.getContent().get("route#").asText()));
        Assert.assertTrue(validator.validate(journey).isEmpty());
    }

    @Test
    public void deserializeInvalidJourney() throws IOException {
        String nometaJson = "{\"journey\":{\"route#\":\"66\",\"coordinates\":[12.22,12.00]}}";

        Journey journey = objectMapper.readValue(nometaJson, Journey.class);

        Assert.assertTrue("66".equals(journey.getContent().get("route#").asText()));
        Assert.assertTrue(!validator.validate(journey).isEmpty());
    }
}
