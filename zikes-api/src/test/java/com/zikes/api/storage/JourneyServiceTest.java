package com.zikes.api.storage;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.google.api.client.util.Lists;
import com.zikes.api.AbstractZikesTest;
import com.zikes.api.model.AccessLevel;
import com.zikes.api.model.Journey;
import com.zikes.api.model.Metadata;
import com.zikes.api.storage.gcs.AccessControlledResourceService;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.model.NotFoundException;

import java.io.IOException;
import java.util.List;

public class JourneyServiceTest extends AbstractZikesTest {

    @Autowired
    AccessControlledResourceService<Journey> journeyService;

    final Long owner = 1L;

    @Test
    public void canSaveJourney() throws IOException {
        Journey journey = createNJourneys(1, owner).get(0);

        journeyService.put(journey);

        Journey journeySaved = journeyService.get(owner, journey.getMetadata().getId());

        Assert.assertTrue(journeySaved.getContent().equals(journey.getContent()));
        Assert.assertEquals(journey.getMetadata().getCreated(), journey.getMetadata().getCreated());
        Assert.assertEquals(journey.getMetadata().getOwner(), owner);
    }

    @Test(expected = NotFoundException.class)
    public void canDelete() throws IOException {
        Journey journey = createNJourneys(1, owner).get(0);

        journeyService.put(journey);

        journeyService.delete(journey);

        journeyService.get(journey.getMetadata().getOwner(), journey.getMetadata().getId());
    }

    @Test
    public void canListJourneys() throws IOException {
        List<Journey> journeyList = createNJourneys(5, owner);

        journeyService.putAll(journeyList);

        List<Metadata> metadataList = journeyService.list(owner);

        Assert.assertEquals(metadataList.size(), journeyList.size());
    }

    private List<Journey> createNJourneys(int n, Long owner) {
        List<Journey> journeyList = Lists.newArrayList();

        for (int i = 0; i < n; i++) {
            Metadata metadata = new Metadata();
            metadata.setId(ZikesIdManager.generateId());
            metadata.setOwner(owner);
            metadata.setAccess(new AccessLevel("public", "public"));

            journeyList.add(new Journey(metadata, JsonNodeFactory.instance.objectNode()));
        }

        return journeyList;
    }
}
