Zikes Front End
================================

Zikes frontend is written in old-school Javascript on top of limited HTML5, Boostrap and whenjs promises.
The dev environment is set up with gulp and browserify.

## Setup
```npm i```

## Develop
```npm run gulp```

## Run tests
```npm run test```

## Distribute (for prod)
```npm run dist```

You should never need to run this step. The parent toolchain (up the directory tree)
should make sure this is done before deploying.
