var assert   = require("assert");
var google   = require("../scripts/lib/google.js");


suite("Geocode", function() {

  suite("basic delta", function() {
    var home = ["GB", "England", "Greater London", "London", undefined, undefined, "Christchurch Ave", "16C"];
    var work = ["GB", "England", "Greater London", "London", undefined, "Marylebone", "All Souls' Pl", "2"];

    test("should identify basic geocode delta", function() {
      assert.deepEqual(google.geocodeHumanReadable([home,work]), ["Christchurch Ave, 16C","All Souls' Pl, 2"]);
    });

    test("should propose a sensible geocode for a single point", function() {
      assert.deepEqual(google.geocodeHumanReadable(home), "GB, London");
      assert.deepEqual(google.geocodeHumanReadable(home,3), "GB, London, Christchurch Ave");

      assert.deepEqual(google.geocodeHumanReadable(
        ["GB", "England", "Greater London", "London", "City of London", undefined, "Old Broad St", "31"], 3
      ), "GB, London, Old Broad St");
      assert.deepEqual(google.geocodeHumanReadable(
        ["PL", "Województwo dolnośląskie", "wrocławski", "Radwanice", "Szostakowice", undefined, "Bzowa", "16"], 3
      ), "PL, Radwanice, Bzowa");
      assert.deepEqual(google.geocodeHumanReadable(
        ["DE", "Thüringen", undefined, "Zeulenroda-Triebes", "Zeulenroda", undefined, "Str. der DSF", "5"], 3
        ), "DE, Zeulenroda-Triebes, Str. der DSF");
    });

    test("should identify delta when the address only differs at a higher level", function() {
      //here this has the same number, but the streets are different.
      var a = ["GB","England","Greater London","London","Soho","Soho","Foubert's Pl","19"];
      var b = ["GB","England","Greater London","London","Soho","Soho","Marshall St","19"];
      assert.deepEqual(google.geocodeHumanReadable([a,b]), ["Foubert's Pl, 19","Marshall St, 19"])
    });

    test("should identify delta when the address only differs slighly", function() {
      //here this has the same number, but the streets are different.
      var a = ["GB","England","Greater London","London",null,null,"Priory Ln",null];
      var b = ["GB","England","Greater London","London",null,null,"Priory Ln","140"];
      assert.deepEqual(google.geocodeHumanReadable([a,b]), ["London, Priory Ln","London, Priory Ln"])
    });

    test("should identify delta when the address only differs slighly", function() {
      //here this has the same number, but the streets are different.
      var a = ["DE","Berlin",null,"Berlin","Mitte",null,"Krausenstraße","14"];
      var b = ["PL","Województwo dolnośląskie","Wrocław","Wrocław","Śródmieście","Ołbin","Wodna",null];
      assert.deepEqual(google.geocodeHumanReadable([a,b]), ["DE, Berlin","PL, Wrocław"])
    });

  });
});