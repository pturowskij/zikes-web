/**
 * scripts/ui/streetView.js
 *
 * This manages the route item in the Journey panel
 */

'use strict';

var when          = require("when");
when["sequence"]  = require('when/sequence');
var mustache      = require("mustache");
var googleAPI     = require("../lib/google.js");
var map           = require("./map.js");
var rotatedMarker = require("leaflet-rotatedmarker");

var streetView = (function() {

    var dialogWindowTemplate = when.defer();
    $.get("templates/ui.template.html", function(template, textStatus, jqXhr) {
        dialogWindowTemplate.resolve($(template).filter("#streetView_tmpl").html());
    });

    function googlePoint2Str(point) {
        return point ? "["+point.lat()+","+point.lng()+"]" : "undefined";
    }

    function latlngEqual(a,b) {
        if (a && b) {
            var a = a instanceof L.LatLng ? [a.lat,a.lng] : [a.lat(),a.lng()];
            var b = b instanceof L.LatLng ? [b.lat,b.lng] : [b.lat(),b.lng()];
            return a[0] == b[0] && a[1] == b[1];
        }
        return false;
    }

    function toGoogle(latlng) {
        return latlng && !(latlng instanceof google.maps.LatLng) ?
            new google.maps.LatLng({lat: latlng.lat, lng: latlng.lng}) : undefined;
    }




    function RoutePoint(sectionIterator) {
        this.mSection = sectionIterator;
    }

    var lastHeading = 0;
    RoutePoint.prototype.heading = function() {
        var current = toGoogle(this.current());
        var next    = toGoogle(this.peekNext({minMetres:5}));
        if (current && next) {
            lastHeading = google.maps.geometry.spherical.computeHeading(
                current,next
            );
        }
        return lastHeading;
    }

    RoutePoint.prototype.current = function() {
        return  this.mSection.current()
    }

    RoutePoint.prototype.next = function(metres) {
        this.mSection.next(metres);
        return this.current();
    }

    RoutePoint.prototype.peekNext = function(metres) {
        return this.mSection.peekNext(metres);
    }

    RoutePoint.prototype.walk = function(stepCb, stoppedCb, options) {
        delete this["mStopped"];
        var self = this;
        options  = options || {
            frameMs  : 2000,
            speedKph : 60
        }
        if (options.maxIterations != undefined) {
            --options.maxIterations;
        } else {
            options.maxIterations = 50;
        }
        if (options.maxIterations > 0 && this.next({metres:(options.speedKph*1000)/(3600000/options.frameMs)})) {
            stepCb(this.current(), this.heading());
            when.delay(options.frameMs)
            .then(function() {
                if (!self.mStopped) {
                    self.walk(stepCb,stoppedCb,options);
                } else {
                    self.stop(stoppedCb);
                }
            });
        } else {
            this.stop(stoppedCb);
        }
    }

    RoutePoint.prototype.stop = function(stoppedCb) {
        this.mStopped = true;
        stoppedCb && stoppedCb();
    }






    var throttleUpdatesToMs = 1000;
    function StreetView() {
    }

    StreetView.prototype.update = function(what) {
        this.shown() && this.show(what);
    }

    StreetView.prototype.show = function(what) {
        var self = this;
        var now = new Date().getTime();
        var dt  = now - this.mLastUpdateMs;
        if (this.mDeferred) {
            clearTimeout(this.mDeferred);
            delete self["mDeferred"];
        }
        if (dt < throttleUpdatesToMs) {
            this.mDeferred = setTimeout(function() {
                self.show(what);
                delete self["mDeferred"];
            }, dt);
            return;
        }
        this.mLastUpdateMs = now;
        if (this.mRoutePoint) {
            this.mRoutePoint.stop();
        }

        var heading = 0;
        if (Array.isArray(what)) {
            what = new L.LatLng(what);
        } else if ("next" in what) {
            this.mRoutePoint = new RoutePoint(what);
            what             = this.mRoutePoint.current();
            heading          = this.mRoutePoint.heading();
        }

        when(this.render(what,heading)).then(function() {
            self.doShow(what, heading);
        });
    }

    function listenForUpdates(self) {
        var streetViewDiv = $("#streetViewWindow #streetViewBody");
        var staticDiv     = $("#streetViewWindow #static");

        var marker = undefined;
        self.mDiv.find(".close").click(function() {
            self.mDiv.addClass("hidden");
            map.leaflet.removeLayer(marker);
            self.mRoutePoint && self.mRoutePoint.stop();
        });

        var markerIcon = L.icon({
            iconUrl:    "graphics/cone.png",
            iconSize:   [40, 40],
            iconAnchor: [20, 40]
        });

        function updateMarker() {
            var streetViewLocation = self.mPanorama.getLocation();
            var position           = self.mPanorama.getPosition();
            if (streetViewLocation &&
                latlngEqual(streetViewLocation.latLng, position)) {
                staticDiv.addClass("hidden");
                streetViewDiv.removeClass("hidden");
            } else {
                streetViewDiv.addClass("hidden");
                staticDiv.removeClass("hidden");
            }
            if (marker) {
                map.leaflet.removeLayer(marker);
            }
            position = new L.LatLng(position.lat(), position.lng());
            var markerPosition = position.distanceTo(self.mRequestedPosition) < 30 ? self.mRequestedPosition : position;
            marker = L.marker(
                markerPosition,
                {
                    icon: markerIcon,
                    draggable : false,
                    opacity: 0.8,
                    rotationAngle: self.mPanorama.getPov().heading
                }
            );
            marker.addTo(map.leaflet);
        }

        var waitingForPano = undefined;
        function panoChanged() {
            var streetViewLocation = self.mPanorama.getLocation();
            var position           = self.mPanorama.getPosition();

            if (streetViewLocation &&
                latlngEqual(streetViewLocation.latLng, position)) {
                updateMarker();
            }
            if (waitingForPano) {
                clearTimeout(waitingForPano)
            }
            waitingForPano = setTimeout(function() {
                updateMarker();
            }, 350)
        }

        self.mPanorama.addListener('position_changed', panoChanged);
        self.mPanorama.addListener('pov_changed', panoChanged);
    }


    StreetView.prototype.render = function(where, heading) {
        if (this.mDiv) {
            this.mDiv.removeClass("hidden");
            return;
        }
        var self = this;
        when(dialogWindowTemplate.promise)
        .then(function(template) {
            $("body").append(
                mustache.render(template)
            );
            self.mDiv = $("#streetViewWindow");
            var streetViewDiv = $("#streetViewWindow #streetViewBody");
            self.mDiv.show();
            self.mPanorama = new google.maps.StreetViewPanorama(
                streetViewDiv[0],
                {
                  position: where,
                  pov: {heading: heading, pitch: 0},
                  zoom: 1
                }
            );

            var walkButton = self.mDiv.find("#walk");
            var stopButton = self.mDiv.find("#stop");
            walkButton.click(function() {
                walkButton.addClass("hidden");
                stopButton.removeClass("hidden");
                self.mRoutePoint.walk(
                    StreetView.prototype.doShow.bind(self),
                    function() {
                        stopButton.addClass("hidden");
                        walkButton.removeClass("hidden");
                    }
                );
            });
            stopButton.click(function() {
                self.mRoutePoint.stop();
            });

            listenForUpdates(self);
        });
    }

    StreetView.prototype.doShow = function(where, heading) {
        this.mRequestedPosition = where;
        this.mPanorama.setOptions({
            position: toGoogle(where),
            pov: {heading: heading, pitch: 0}
        });
    }

    StreetView.prototype.shown = function() {
        return this.mDiv ? !this.mDiv.hasClass("hidden") : false;
    }

    return new StreetView();

})();

module.exports = streetView;