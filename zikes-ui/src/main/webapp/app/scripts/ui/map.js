/**
 * scripts/ui/map.js
 *
 * This prepares and manages the leaflet map element
 */

'use strict';

var when      = require("when");
when.delay    = require("when/delay");
                require("../external/leaflet-google.js");
                require("../external/leaflet-singleclick.js");
var zikesApi  = require("../lib/zikesRouter.js");
var googleAPI = require("../lib/google.js");
var geoip     = require("../lib/geoip.js");
var messages  = require("./messages.js");
var ctxMenu   = require("./contextMenu.js");

var map = (function() {

    var mapElement = $("#map");
    var mapWrap    = $("#mapWrap")
    var openStreetMap = L.tileLayer('http://otile{s}.mqcdn.com/tiles/1.0.0/osm/{z}/{x}/{y}.png', {
        maxZoom: 18,
        subdomains : "1234",
        attribution: 'Tiles Courtesy of <a href="http://www.mapquest.com/" target="_blank">MapQuest</a> <img src="http://developer.mapquest.com/content/osm/mq_logo.png">'
    });

    var openCycleMap = L.tileLayer('http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>'
    });


    var initialCentre = new L.LatLng(49.977722, 14.064453);
    var googleAerial = new L.Google();

    var highwaysById      = undefined;
    var highwaysByName    = undefined;
    var highlightColor    = "color=\"40E0D0\"";
    var map = L.map('map', {
      center: initialCentre,
      zoom: 5,
      layers: [openStreetMap]
    });
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        var message = "<span class=\"lead\">This website is for trip <font "+highlightColor+">planning</font>, which we think is best done from home.</span><br>"
            +"<span class=\"small\"><font color=\"#C0C0C0\">And although some of it could be done away from home, we could only prioritise the big browser experience. We regret to bring this up, but</font></span><br>"
            +"<span class=\"lead\"><font "+highlightColor+">This website is not mobile ready.</font></span><br>";
            if (/Android|Opera Mini/i.test(navigator.userAgent)) {
                message += "<span class=\"small\">If you're already a user and would like to access your trips. If you're lost, unsure how to get home and/or are after the default, "
                          +"lightweigth <font "+highlightColor+">Zikes</font> experience, why not install our <a href=\"https://dl.dropboxusercontent.com/u/11723457/Zikes/net.Zikes.apk\">Android App</a>.</span><br>";
            }

        messages.popup(message, {indismissable:true}).enqueue();
        $(".navbar").hide();
        $("body").removeClass("room-for-navbar");
        $("#mainWorkArea").css("padding", "0");
        return
    }
    var welcomeMessage    = messages.popup(
        " <span  class=\"lead\"><font "+highlightColor+">Zikes</font> is here so you can be <font "+highlightColor+">fastidious</font> about which way you walk or ride when you fancy walking or riding.</span><br>"
        +" <span class=\"small\"><font color=\"#C0C0C0\">We're renting a small (32GB) server in Germany and can't yet afford serving the whole world. What we don't serve we cover with a pinkish overlay.<br>"
        +" We will use these popups rarely and try communicating with you unintrusivelly via the panel directly below.</font></span><br>"
        +" Go on, interact with the map. <font "+highlightColor+">You need to specify 'to' and 'from'</font> (both by clicking) to have us plot a route for you."
        +" If you're unsure where to click, use the <font "+highlightColor+">'find a place'</font> box in the top left corner.",
        {once:"welcome"}
    );
    var connectionMessage = messages.wait(
        "Contacting backend... It sometimes needs waking up. This normally takes less than 20s."
    );
    var geoIpRetrieved    = geoip.get();
    when.delay(1000).then(function() {
        connectionMessage.enqueue();
        if (!highwaysByName) {
            busy(true);
        }
    });
    zikesApi.meta()
    .then(function(meta) {
        busy(false);
        connectionMessage.hide();
        var coverageOverlay = [[[89, -179],[89, 179],[-89, 179],[-89, -179]]];
        highwaysByName = meta.highways;
        highwaysById = {};
        for (var name in highwaysByName) {
            var id = highwaysByName[name];
            highwaysById[id] = name;
        }
        coverageOverlay = coverageOverlay.concat(meta.coverage);
        var polygon = L.polygon(
            coverageOverlay,
            {
                color: "red",
                weight: 0,
                clickable: false
            }
        ).addTo(map);
        if (!welcomeMessage.hidden()) {
            when.delay(2000)
            .then(function() {
                welcomeMessage.enqueue();
            });

            return welcomeMessage
            .whenShown()
            .delay(3000)
            .then(function() {
                return meta.coverage;
            });
        }
        return meta.coverage;
    })
    .otherwise(function() {
        connectionMessage.hide();
        messages.error(
            "We're sorry, but our routing server appears to be down :( There is nothing we can do for you right now.",
            {timeoutSec:0}
        ).enqueue();
        throw "We're doomed!"
    })
    .then(function(coverage) {
        return geoIpRetrieved.then(function(geoipData) {
            if (geoipData.country != "") {
                var currentCentre = map.getCenter();
                if (pips(geoipData.latlng, coverage)) {
                    if (currentCentre.lat == initialCentre.lat && currentCentre.lon == initialCentre.lon) {
                        map.setView(
                            geoipData.latlng,
                            7,
                            { animate : true }
                        );
                    }
                } else {
                    messages.warn(
                        "We're sorry, but it appears, where we think you are ("+geoipData.country+"), we don't offer coverage :( We only have a small server."
                    ).enqueue();
                }
            }
        })
    });


    var baseMaps = {
      "OpenCycleMap": openCycleMap,
      "OpenStreetMap": openStreetMap,
      "Google Aerial": googleAerial
    };

    L.control.layers(baseMaps).addTo(map);

    map.on('singleclick', function(e) {
        if (!mapElement.hasClass("busy")) {
            ctxMenu(e.latlng).default().show();
        }
    });
    map.on('dblclick', function(e) {
        map.zoomIn();
    });

    function expand() {
        if (mapWrap.hasClass("shrunk")) {
            var done = when.defer();
            mapWrap.removeClass('shrunk').animate({'margin-left':'0px'}, function() {
                done.resolve();
            });
            return done.promise;
        }
        return when(true);
    }

    function shrink() {
        if (!mapWrap.hasClass("shrunk")) {
            var done = when.defer();
            mapWrap.addClass('shrunk').animate({'margin-left':'300px'}, function() {
                done.resolve();
            });
            return done.promise;
        }
        return when(true);
    }

    function isShrunk() {
        return mapWrap.hasClass("shrunk");
    }

    function busy(busy) {
        if (busy) {
            mapElement.css("cursor", "wait");
        } else {
            mapElement.css("cursor", "");
        }
    }

    var searchMarkers = [];
    function search(text) {
        for (var i = 0; i < searchMarkers.length; i++) {
            map.removeLayer(searchMarkers[i]);
        }
        searchMarkers = [];
        googleAPI.geocodeFind(text)
        .then(function(results) {
            if (results && results.length) {
                var bounds = new L.latLngBounds([]);
                for (var i = 0; i < results.length; i++) {
                    var result = results[i];
                    bounds.extend(new L.LatLng(
                        result.geometry.viewport.northeast.lat, result.geometry.viewport.northeast.lng)
                    );
                    bounds.extend(new L.LatLng(
                        result.geometry.viewport.southwest.lat, result.geometry.viewport.southwest.lng)
                    );

                    var marker = L.marker(new L.LatLng(
                        result.geometry.location.lat, result.geometry.location.lng),
                        { draggable : false});
                    marker.addTo(map);
                    searchMarkers.push(marker);
                }

                map.fitBounds(bounds, {animate:true});
            }
        });
    }

    function pip(point, polygon) {
        // ray-casting algorithm based on
        // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html

        var x = point.lat, y = point.lng;

        var inside = false;
        for (var i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
            var xi = polygon[i][0], yi = polygon[i][1];
            var xj = polygon[j][0], yj = polygon[j][1];

            var intersect = ((yi > y) != (yj > y))
                && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
            if (intersect) inside = !inside;
        }

        return inside;
    };

    function pips(point, polygons) {
        for (var i = 0; i < polygons.length; i++) {
            if (pip(point, polygons[i])) {
                return true;
            }
        }
        return false;
    }

    return {
        expand          : expand,
        shrink          : shrink,
        isShrunk        : isShrunk,
        leaflet         : map,
        elem            : mapElement,
        search          : search,
        busy            : busy,
        highwaysByName  : function() { return highwaysByName; },
        highwaysById    : function() { return highwaysById; }
    };

})();

module.exports   = map;