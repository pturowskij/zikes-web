/**
 * scripts/ui/contextMenu.js
 *
 * This prepares and manages the Zikes's UI.
 */

'use strict';

var contextMenu = (function() {

    function ContextMenu(latlng) {
    	this.mLatLng     = latlng;
    	this.mActions    = [];
    	this.mLeafletMap = require("./map.js").leaflet; //bit of dependency cycle
    	this.mCurJourney = require("./ui.js").journeys.current();
        this.mStreetView = require("./streetView.js");
    }

    ContextMenu.prototype.from = function(options) {
    	var self = this;
		this.action({
    		id     : "from",
    		display: "Route From",
    		action : function() {
    			self.mCurJourney.pendingRoute().from(self.mLatLng);
    		}
    	}, options);
		return this;
    }

    ContextMenu.prototype.to = function(options) {
    	var self = this;
		this.action({
    		id     : "to",
    		display: "Route To",
    		action : function() {
    			self.mCurJourney.pendingRoute().to(self.mLatLng);
    		}
    	}, options);
		return this;
    }

    ContextMenu.prototype.poi = function(options) {
    	var self = this;
		this.action({
    		id     : "poi",
    		display: "New Point of Interest",
    		action : function() {
    			self.mCurJourney.newPOI(self.mLatLng);
    		}
    	}, options);
		return this;
    }

    ContextMenu.prototype.delete = function(options) {
    	var self = this;
		this.action({
    		id     : "delete",
    		display: "Delete",
            action : undefined
    	}, options);
		return this;
    }

    ContextMenu.prototype.streetView = function(options) {
        var self = this;
    	var route = options ? options.route : undefined;
    	if ( route && this.mStreetView.shown()) {
    		return this;
    	}
    	this.action({
    		id     : "streetView",
    		display: "Street View",
    		action : function() {
    			self.mStreetView.show(route || self.mLatLng);
    		}
    	}, options);
		return this;
    }

    ContextMenu.prototype.separator = function() {
    	this.action({
    		html : "<hr style=\"margin:2px 0px 12px 0px\">",
            separator: true
    	});
		return this;
    }

    ContextMenu.prototype.action = function(template, options) {
        var copiedOptions = {};
		for (var key in options) {
            copiedOptions[key] = options[key];
        }
        for (var key in template) {
            copiedOptions[key] = copiedOptions[key] || template[key];
        }
        for (var i = 0; i < this.mActions.length; i++) {
            if (this.mActions[i].id && this.mActions[i].id == copiedOptions.id) {
                if (copiedOptions.delete) {
                    this.mActions.splice(i,1);
                } else {
                    this.mActions[i] = copiedOptions;
                }
                return;
            }
        }
        if (!copiedOptions.delete) {
            this.mActions.push(copiedOptions);
        }
		return this;
    }

    ContextMenu.prototype.default = function() {
		return this.mCurJourney.pendingRoute().fillContextMenu(
            this.streetView().separator().poi()
        );
    }

    ContextMenu.prototype.show = function() {
    	var self = this;
    	var html = this.mActions.map(function(elem,i,arr) {
            if ((i == arr.length-1 || i == 0) && elem.separator == true) {
                return "";
            }
            var inner = elem.display;
            if (elem.bold) {
                inner = "<b>"+inner+"</b>";
            }
    		return elem.html || '<div id="'+elem.id+'"><a href="#">'+inner+'</a></div>'
    	}).join("");
        var c = document.createElement('div');
        c.innerHTML=html;
        var popup = L.popup()
            .setLatLng(this.mLatLng)
            .setContent(c)
            .openOn(this.mLeafletMap);
		this.mActions.map(function(elem) {
            $(c).find("#"+elem.id).click(function() {
                elem.action();
                self.mLeafletMap.removeLayer(popup);
            });
    	});
    }

    return ContextMenu;

})();

module.exports  = function(latlng) { return new contextMenu(latlng) };