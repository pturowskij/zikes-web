/**
 * scripts/ui/panelRoute.js
 *
 * This manages the route item in the Journey panel
 */

'use strict';

var when          = require("when");

var googleAPI     = require("../lib/google.js");
var utils         = require("../lib/utils.js");
var highways      = require("../lib/highways.js").highways;

var map           = require("./map.js");
var mapRoute      = require("./mapRoute.js");
var messages      = require("./messages.js");

var journeyItem   = require("./journeyItem.js");
var JourneyItem   = journeyItem.JourneyItem;
var journeyTemplates = journeyItem.templates;

var panelRoute = (function() {

    function Route(parent, panelDiv) {
        JourneyItem.call(this, parent, panelDiv);
        this.mParams["from"] = {};
        this.mParams["to"] = {};

        var self = this;
        this.mRouteOnMap = new mapRoute.Route({
            highlight : function(milestoneMts) {
                if (milestoneMts) {
                    self.show();
                }
                highlightMilestone(self,milestoneMts);
            },
            invalidate : function() {
                return self.render().then(function() {
                    self.from(self.mRouteOnMap.from());
                    self.to(self.mRouteOnMap.to());
                    var profile   = self.mRouteOnMap.getProfile();
                    self.mProfile = undefined;
                    if (profile) {
                        self.mProfile = prepareProfile(
                            profile,
                            self.mDivs.profileCanvas.width/4, //px resolution
                            self.mDivs.profileCanvas.height
                        );
                    }
                    return self.invalidate();
                })
            },
            route : function(milestones) {
                var beforeMts = new Date().getTime();
                map.busy(true);
                self.busy(true);
                var routed = self.mParent.route(milestones);

                routed.then(function(route) {
                    var serverRoutingMs  = route.meta.routing_us/1000;
                    var networkLatencyMs = (new Date().getTime() - beforeMts)-serverRoutingMs;
                    var visitedJunctions = route.meta.routingRequests.reduce(function(cum, cur) {return cum+cur.visitedJunctions}, 0);
                    messages.info(
                        "To plot this route we visited "+utils.largeNumberToStr(visitedJunctions)
                         +" junctions and it took us "+utils.millisToString(serverRoutingMs)+" to do so. It took us additional "
                         +utils.millisToString(networkLatencyMs)+" to send this result accross the wire."
                    ).enqueue();
                })
                .catch(function(err) {
                    var message = undefined;
                    if (typeof err.reason === 'object') {
                        if (err.reason.code == "RoutingException::NearestPointNotFound") {
                            message = "Unable to find a road/path within "+err.reason.attemptedToleranceMts+"mts from where you clicked. Wiggle that a bit and position it nearer a way please.";
                            map.leaflet.setView(
                                err.reason.culprit,
                                13,
                                { animate: true }
                            );
                        } else if (err.reason.code == "RoutingException::ServerOverload") {
                            message = "We've analysed " + utils.largeNumberToStr(err.reason.junctionsLimitExceeded) + " junctions for you and had to to bail to protect our small server from swapping RAM. Try plotting a shorter route.";
                        } else if (err.reason.code == "RoutingException::NoRoute" ) {
                            message = "We're sorry, but we could not find a route between the points you requested.";
                        }
                    }
                    messages.error(
                        message || "We're sorry, there's been an unusual problem. Our server says :'"+err.message+"'"
                    ).enqueue();
                })
                .ensure(function() {
                    map.busy(false);
                    self.busy(false);
                }).done();
                return routed;
            }
        });
    }
    Route.prototype = new JourneyItem();
    Route.prototype.constructor = Route;



    function updateGeocodedLocation(self, latlng, toOrFromStr) {
        if (latlng instanceof L.LatLng) {
            latlng = [latlng.lat, latlng.lng]
        }
        var storedAs = self.mParams[toOrFromStr];
        if (!storedAs.latlng ||
            !(latlng[0] == storedAs.latlng[0] && latlng[1] == storedAs.latlng[1])) {
            storedAs.latlng = latlng;
            googleAPI.reverseGeocodeFind(latlng)
            .then(function(geocode) {
                storedAs.geocode = geocode;
                storedAs.latlng = latlng;
                self.invalidate();
            });
        }
    }

    Route.prototype.from = function(latlng) {
        if (!latlng) {
            //getter
            return this.mParams.from;
        }
        updateGeocodedLocation(this, latlng, "from");
        if (this.mRouteOnMap.from(latlng)) {
            this.mRouteOnMap.invalidate();
        }
    }

    Route.prototype.to = function(latlng) {
        if (!latlng) {
            //getter
            return this.mParams.to;
        }
        updateGeocodedLocation(this, latlng, "to");
        if (this.mRouteOnMap.to(latlng)) {
            this.mRouteOnMap.invalidate();
        }
    }

    Route.prototype.route = function() {
        this.mRouteOnMap.invalidate(true);
    }

    Route.prototype.fillContextMenu = function(ctxMenu) {
        if (this.mRouteOnMap.from()) {
            //from set
            ctxMenu.from({
                display: "Change From"
            }).to({
                bold: true
            });
        } else {
            ctxMenu.to().from({
                bold: true
            });
        }
        return ctxMenu;
    }

    Route.prototype.name = function() {
        var result = JourneyItem.prototype.name.call(this);
        if (!result) {
            if ((this.mParams.from.geocode || this.mParams.to.geocode)) {
                var fromTo = ["",""];
                if (this.mParams.from.geocode && this.mParams.to.geocode) {
                    fromTo = googleAPI.geocodeHumanReadable(
                        [this.mParams.from.geocode,
                         this.mParams.to.geocode]
                    );
                } else if (this.mParams.from.geocode) {
                    fromTo[0] = googleAPI.geocodeHumanReadable(
                        this.mParams.from.geocode
                    );
                } else {
                    fromTo[1] = googleAPI.geocodeHumanReadable(
                        this.mParams.to.geocode
                    );
                }
                result = fromTo[0] + " -> " + fromTo[1];
            } else {
                result = "New Route";
            }
        }
        return result;
    }

    Route.prototype.distanceMts = function() {
        if (this.mRouteOnMap) {
            return this.mRouteOnMap.getDistanceMts();
        }
        return 0;
    }

    Route.prototype.isPending = function() {
        return this.mRouteOnMap.isPending();
    }

    Route.prototype.delete = function() {
        JourneyItem.prototype.delete.call(this);
        this.mRouteOnMap.delete();
    }

    function resetDelta(self) {
        var deltaChar = "\u0394";
        var content = self.mProfile ? (deltaChar + self.mProfile.minMaxEle[2]) + "mts" : "";
        self.mDivs.dEle.text(content);
        self.mDivs.dEle.css("margin-left", "0px");
    }


    Route.prototype.doInvalidate = function() {
        if (!this.mRouteOnMap || (!this.mRouteOnMap.from() && !this.mRouteOnMap.to())) {
            this.delete();
            return;
        }
        JourneyItem.prototype.doInvalidate.call(this);
        this.mDivs.distance.text(utils.mtsToString(this.distanceMts()));
        drawProfile(this.mDivs.profileCanvas, this.mProfile);
        resetDelta(this);
    }

    Route.prototype.toJSON = function() {
        return {
            meta: this.mParams,
            sections: this.mRouteOnMap.toJSON()
        };
    }

    Route.prototype.fromJSON = function(json) {
        var self = this;
        this.mParams = json.meta;
        this.show = function() { //prevent expanding accordion when deserialising
            delete self["show"];
            return when(true);
        }
        return this.mRouteOnMap.fromJSON(json.sections);
    }

    Route.prototype.busy = function(busy) {
        if (this.mDivs) {
            if (busy) {
                this.mDivs.centreIcon.addClass("hide");
                this.mDivs.busyIcon.removeClass("hide")
            } else {
                this.mDivs.busyIcon.addClass("hide");
                this.mDivs.centreIcon.removeClass("hide")
            }
        }
    }

    Route.prototype.getBounds = function() {
        return this.mRouteOnMap.getBounds();
    }

    Route.prototype.toGpx = function() {
        var result = '<rte name="'+this.name()+'">\n';
        result += '  '+this.mRouteOnMap.gpxRtePts() + '\n</rte>';
        return result;
    }

    Route.prototype.render = function() {
        var self = this;
        return JourneyItem.prototype.render.call(
            this,
            "Route",
            {
                distance: utils.mtsToString(self.distanceMts()),
                dEle: ""
            }
        )
        .then(function() {
            self.mDivs["profileCanvas"]   = self.mDivs.collapsiblePanel.find(".routeProfileCanvas").get(0),
            self.mDivs["highlightCanvas"] = self.mDivs.collapsiblePanel.find(".highlightProfileCanvas").get(0),
            self.mDivs["distance"]        = self.mDivs.collapsiblePanel.find(".routeDistance"),
            self.mDivs["dEle"]            = self.mDivs.collapsiblePanel.find(".routeEleDelta"),
            self.mDivs.name.hover(
                function() { self.mRouteOnMap.highlight(true); },
                function() { self.mRouteOnMap.highlight(false); }
            );
        });
    }


    function prepareProfile(profile, reduceToDatapoints, canvasHeight) {
        var distanceMts    = profile[profile.length-1].distanceFromStartMts;
        var trimToMts      = distanceMts/reduceToDatapoints;
        var prev           = undefined;
        var accumDistance  = 0;
        var minMaxEle      = [profile[0].eleMts, profile[0].eleMts];
        var climbBuckets   = {
            prev            : undefined,
            accumDistance   : 0,
            minDistance     : 100,
            modulo          : 3 //percent buckets
        };
        var dstProfile     = [{
            distanceMts     : 0,
            eleMts          : profile[0].eleMts
        },{
            distanceMts     : 0,
            minMaxEle       : minMaxEle,
            highways        : {}
        }];

        function finaliseNode(dstProfileNode, prevDstProfileNode) {
            var mostPopularHighway = undefined;
            for (var highway in dstProfileNode.highways) {
                if (!mostPopularHighway || mostPopularHighway.distanceMts < dstProfileNode.highways[highway]) {
                    mostPopularHighway = {
                        highway     : highway,
                        distanceMts : dstProfileNode.highways[highway]
                    }
                }
            }
            dstProfileNode["highway"] = parseInt(mostPopularHighway.highway);
            dstProfileNode["eleMts"]  =
                Math.abs(prevDstProfileNode.eleMts - dstProfileNode.minMaxEle[0]) > Math.abs(prevDstProfileNode.eleMts - dstProfileNode.minMaxEle[1]) ?
                    dstProfileNode.minMaxEle[0] :
                    dstProfileNode.minMaxEle[1];

            delete dstProfileNode["minMaxEle"];
            delete dstProfileNode["highways"];
        }

        for (var i = 0; i < profile.length; i++) {
            var srcProfileNode = profile[i];
            accumDistance     += srcProfileNode.fromPrevJntnMts;
            minMaxEle[0]       = Math.min(minMaxEle[0], srcProfileNode.eleMts);
            minMaxEle[1]       = Math.max(minMaxEle[1], srcProfileNode.eleMts);

            var dstProfileNode = dstProfile[dstProfile.length-1];
            dstProfileNode.distanceMts += srcProfileNode.fromPrevJntnMts;
            var slope = 1.0;
            if (prev) {
                if (prev.highway != undefined) { //highways are projecting forwards from each input profile junction
                    if (!(prev.highway in dstProfileNode["highways"])) {
                        dstProfileNode.highways[prev.highway] = 0;
                    }
                    dstProfileNode.highways[prev.highway] += srcProfileNode.fromPrevJntnMts;
                }
                //slope is looking back (as the while loop that follows divides the previous chunk into smaller bites)
                slope = (srcProfileNode.eleMts - prev.eleMts)/srcProfileNode.fromPrevJntnMts;
            }
            while (dstProfileNode.distanceMts >= trimToMts) {
                //enough to commit a datapoint
                var next = { //this goes to the next datapoint
                    distanceMts : dstProfileNode.distanceMts - trimToMts,
                    highways : {}
                }
                var eleMtsHere = (srcProfileNode.eleMts - ((srcProfileNode.fromPrevJntnMts - next.distanceMts) * slope)) | 0;
                next["minMaxEle"] = [eleMtsHere, eleMtsHere];

                dstProfileNode.minMaxEle[0]     = Math.min(dstProfileNode.minMaxEle[0], eleMtsHere);
                dstProfileNode.minMaxEle[1]     = Math.max(dstProfileNode.minMaxEle[1], eleMtsHere);
                dstProfileNode.distanceMts      = trimToMts;
                finaliseNode(dstProfileNode, dstProfile[dstProfile.length-2]);
                dstProfile.push(next);
                var highway = srcProfileNode.highway || dstProfileNode.highway;
                next.highways[highway] = next.distanceMts;
                dstProfileNode = next;
            }

            dstProfileNode.minMaxEle[0] = Math.min(dstProfileNode.minMaxEle[0], srcProfileNode.eleMts);
            dstProfileNode.minMaxEle[1] = Math.max(dstProfileNode.minMaxEle[1], srcProfileNode.eleMts);


            climbBuckets.accumDistance += srcProfileNode.distanceMts;
            if (climbBuckets.accumDistance > climbBuckets.minDistance) {
                var prevClimb = climbBuckets.prev ? climbBuckets.prev : prev;
                if (prevClimb) {
                    var climbPercent = ((((srcProfileNode.eleMts - prevClimb.eleMts)/(climbBuckets.modulo*climbBuckets.accumDistance))*100) | 0) * climbBuckets.modulo;
                    if (!climbBuckets[climbPercent]) {
                        climbBuckets[climbPercent] = {
                            climbPercent : climbPercent,
                            distanceMts  : 0
                        };
                    }
                    climbBuckets[climbPercent].distanceMts += climbBuckets.accumDistance;
                    climbBuckets.prev = srcProfileNode;
                    climbBuckets.accumDistance = 0;
                }
            }
            prev = srcProfileNode;
        }
        var prevDstProfileNode = dstProfile[dstProfile.length-2];
        dstProfileNode.highways[prevDstProfileNode.highway] = dstProfileNode.distanceMts;
        finaliseNode(dstProfile[dstProfile.length-1], prevDstProfileNode);

        //now prepare pixel values for the y coordinate
        var dEle = minMaxEle[1] - minMaxEle[0];
        minMaxEle.push(dEle);

        //don't dillute your resultion - assume at least 150mts elevation difference at y.
        dEle = Math.max(dEle, 150);
        var elevateY = canvasHeight/3 | 0;
        var yFactor  = 1.0 * (2*elevateY)/dEle;

        for (var i = 0; i < dstProfile.length; i++) {
            dstProfile[i]["y"] = canvasHeight - (elevateY + yFactor * (dstProfile[i].eleMts - minMaxEle[0])) | 0;
        }

        return {
            profile      : dstProfile,
            climbBuckets : climbBuckets,
            minMaxEle    : minMaxEle,
            distanceMts  : distanceMts
        }
    }

    function drawProfile(canvas, profile) {
        var c2 = canvas.getContext("2d");
        c2.clearRect(0, 0, canvas.width, canvas.height);
        if (!profile) {
            return;
        }
        var xFactor  = Math.ceil(canvas.width/profile.profile.length);
        c2.fillStyle = "#5bc0de";
        c2.beginPath();
        var x = 0;
        c2.moveTo(x, profile.profile[0].y);
        for (var i = 1; i < profile.profile.length; i++ ) {
            var node = profile.profile[i];
            x = i*xFactor;
            if (i == profile.profile.length-1) {
                x = canvas.width;
            }
            c2.lineTo(x, node.y);
        }
        c2.lineTo(canvas.width, canvas.height);
        c2.lineTo(0,canvas.height);
        c2.closePath();
        c2.fill();

        var prevX = 0;
        for (var i = 1; i < profile.profile.length; i++ ) {
            var node = profile.profile[i];
            x = i*xFactor;
            if (i == profile.profile.length-1) {
                x = canvas.width;
            }

            c2.beginPath();
            c2.rect(prevX, canvas.height-4, x, canvas.height);
            c2.fillStyle = highways.highway(node.highway).color();
            c2.fill();
            prevX = x;
        }
    }

    var highlightDrawn = false;
    function highlightMilestone(self, milestoneMts) {
        var canvas = self.mDivs.highlightCanvas
        var c2     = canvas.getContext("2d");
        if (milestoneMts === false) {
            highlightDrawn = false;
            setTimeout(function() {
                if (!highlightDrawn) {
                    c2.clearRect(0, 0, canvas.width, canvas.height);
                    resetDelta(self);
                }
            }, 1000);

        } else {
            highlightDrawn = true;
            c2.clearRect(0, 0, canvas.width, canvas.height);
            var profileIdx  = Math.min(
                Math.round(self.mProfile.profile.length*milestoneMts/self.mProfile.distanceMts),
                self.mProfile.profile.length-1
            );
            var bottom          = canvas.height-15;
            var x               = profileIdx*Math.ceil(canvas.width/self.mProfile.profile.length);
            var y               = self.mProfile.profile[profileIdx].y;
            c2.beginPath();
            c2.moveTo(x-1, y);
            c2.lineTo(x+1, y);
            c2.lineTo(x+1, bottom-1);
            c2.lineTo(x-1, bottom-1);
            c2.fillStyle = "#ebebeb";
            c2.closePath();
            c2.fill();
            var halfCanvasWidth = canvas.width/2;
            var dEleX = Math.max(
                Math.min(
                    x-halfCanvasWidth,
                    halfCanvasWidth-22
                ), 22-halfCanvasWidth
            ) - 2;
            self.mDivs.dEle.css(
                "margin-left",
                dEleX+"px"
            );
            self.mDivs.dEle.text(self.mProfile.profile[profileIdx].eleMts + "mts");
        }
    }

    return { Route : Route };

})();

module.exports = panelRoute;