/**
 * scripts/ui/routingPreference.js
 *
 * This prepares and manages the Routing Preference UI panel
 */
'use strict';

var when     = require("when");
var mustache = require("mustache");
var map      = require("./map.js");
var zikesApi = require("../lib/zikesRouter.js");
               require("../external/bootstrap-slider.js");

var routingPreference = (function() {

    var sliderTemplate = when.defer();
    $.get("templates/tools.template.html", function(template, textStatus, jqXhr) {
        var html = $(template).filter("#routingPreferenceSlider_tmpl").html()
        sliderTemplate.resolve(html);
    });
    var toolAccordionElemTemplate = when.defer();
    $.get("templates/tools.template.html", function(template, textStatus, jqXhr) {
        var html = $(template).filter("#routingPreferenceAccordionElem_tmpl").html()
        toolAccordionElemTemplate.resolve(html);
    });
    var trafficRestrictionsTemplate = when.defer();
    $.get("templates/tools.template.html", function(template, textStatus, jqXhr) {
        var html = $(template).filter("#routingPreferenceTrafficRestrictions_tmpl").html()
        trafficRestrictionsTemplate.resolve(html);
    });

    function RoutingPreference(updateCb, jsonDocument) {
        this.mUpdatedCb  = updateCb;
        this.mJSON       = jsonDocument;
        this.mSliders    = []
        this.mProfiles = {
            public : {},
            private: {}
        };
        this.mSliders.push(new Slider(
            this,
            require("../lib/highways.js").sliderGenerator
        ));
        this.mSliders.push(new Slider(
            this,
            require("../lib/climb.js").sliderGenerator
        ));
        this.mSliders.push(new Slider(
            this,
            require("../lib/turns.js").sliderGenerator,
            {classs: "routingLeftPreference"}
        ));
        this.mSliders.push(new Slider(
            this,
            require("../lib/cycleways.js").sliderGenerator,
            {classs: "routingRightPreference"}
        ));
        this.mSliders.push(new Slider(
            this,
            require("../lib/urbanRural.js").sliderGenerator
        ));
        this.mSliders.push(new Slider(
            this,
            require("../lib/popularity.js").sliderGenerator
        ));

        var self = this;
        $("#saveProfile").click(function() {
            var newProfile = self.toJSON($("#presetProfilesInput").val());
            zikesApi.saveProfile(newProfile);
            self.mProfiles.private[newProfile.metadata.opaque.name] = newProfile.preferences;
            $("#deleteProfile").removeClass("disabled");
            self.mLastProfile = newProfile.metadata.opaque.name;
            zikesApi.preferences({
                lastProfile: self.mLastProfile
            });
            resetTypeahead(self);
        });

        $("#deleteProfile").click(function() {
            var profileName = $("#presetProfilesInput").val();
            self.loadProfile()
            zikesApi.deleteProfile(profileName);
            delete self.mProfiles.private[profileName];
            resetTypeahead(self);
        });
        this.render($("#toolsAccordion"));
    }

    var profilesTypeaheadMatcher = function(self) {
        var KMaxListLength = 10;
        return function findMatches(q, cb) {
            var profileNames = Object.keys(self.mProfiles.public).concat(Object.keys(self.mProfiles.private));
            if (q == "" || profileNames.length <= KMaxListLength) {
                cb(profileNames.slice(
                    0, Math.min(KMaxListLength, profileNames.length)
                ));
                return;
            }

            var matches, substrRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function(i, str) {
                if (substrRegex.test(str)) {
                    matches.push(str);
                }
            });

            cb(matches);
        };
    };

    function resetTypeahead(self) {
        var input = $("#presetProfilesInput");
        input.typeahead("destroy");
        input.typeahead({
            minLength: 0,
            highlight: true},
            { source:profilesTypeaheadMatcher(self)}
        );
    }

    RoutingPreference.prototype.render = function(panel) {
        var self     = this;
        var slidersRendered = [];
        for (var i = 0; i < self.mSliders.length; i++) {
            slidersRendered.push(
                self.mSliders[i].render(panel)
            );
        }

        var meansButtons = $("#meansPanel .btn");
        meansButtons.on("click", function() {
            var clicked = this;
            meansButtons.each(function() {
                if (this != clicked && $(this).hasClass("active")) {
                    $(this).button("toggle");
                }
            });
        });

        return when.all(slidersRendered)
        .then(function() {
            var publicProfilesFetched = zikesApi.publicProfiles()
            .then(function(profiles) {
                self.mProfiles.public = profiles;
                if (!self.mLastProfile) {
                    self.loadProfile(self.mProfiles.public[0]);
                }
                return profiles;
            });
            var privateProfilesFetched = zikesApi.profiles(false)
            .then(function(profiles) {
                self.mProfiles.private = profiles;
                return zikesApi.preferences();
            })
            .then(function(preferences) {
                return self.mHasBeenFiddledWith || self.loadProfile(preferences.lastProfile);
            });

            when.any([publicProfilesFetched, privateProfilesFetched])
            .then(function(profiles) {
                var input = $("#presetProfilesInput");
                resetTypeahead(self);

                input.on("typeahead:selected", function (e) {
                    self.loadProfile(input.val());
                }).on("typeahead:autocompleted", function (e) {
                    self.loadProfile(input.val());
                }).on("keypress", function (e) {
                    if(event.which === 13) {
                        self.loadProfile(input.val());
                    }
                }).on("blur", function() {
                    if (input.val() == "") {
                        input.val(self.mLastProfile);
                    }
                });
            });

            return when.all(
                slidersRendered.concat([publicProfilesFetched, privateProfilesFetched])
            );
        });
    }

    RoutingPreference.prototype.generatePreference = function() {
        var preference = {
            means: "bicycle"
        };
        this.mSliders.forEach(function(slider) {
            slider.generatePreference(preference);
        });
        return preference;
    }

    RoutingPreference.prototype.invalidate = function() {
        this.mHasBeenFiddledWith = true;
        this.mUpdatedCb();
    }

    RoutingPreference.prototype.loadProfile = function(name) {
        var publicProfileNames = Object.keys(this.mProfiles.public);
        if (name == undefined || name == "") {
            //prevent empty names
            if (this.mLastProfile) {
                name = this.mLastProfile;
            } else if (publicProfileNames.length) {
                name = publicProfileNames[0]
            }
        }
        if (name in this.mProfiles.private) {
            this.fromJSON(
                this.mProfiles.private[name]
            );
            $("#deleteProfile").removeClass("disabled");
        } else {
            this.fromJSON(
                this.mProfiles.public[name]
            );
            $("#deleteProfile").addClass("disabled");
        }
        $("#presetProfilesInput").val(name);
        if (zikesApi.isLoggedIn()) {
            zikesApi.preferences({lastProfile: name});
        }
        this.mLastProfile = name;
    }

    RoutingPreference.prototype.fromJSON = function(json) {
        for (var i = 0; i < this.mSliders.length; i++) {
            var slider = this.mSliders[i];
            var sliderJSON = (json && json.sliders) ? json.sliders[slider.label()] : undefined;
            slider.fromJSON(sliderJSON);
        }

        json.means = json.means || "meansIgnoreRestrictions";
        $("#meansPanel .btn").each(function() {
            if ($(this).attr('id') == json.means) {
                if (!$(this).hasClass("active")) {
                    $(this).button("toggle");
                }
            } else if ($(this).hasClass("active")) {
                $(this).button("toggle");
            }
        });
    }

    RoutingPreference.prototype.toJSON = function(profileName) {
        var means;
        $("#meansPanel .btn").each(function() {
            if ($(this).hasClass("active")) {
                means = $(this).attr("id");
            }
        });
        var json = {
            sliders : {},
            means : means
        };
        for (var i = 0; i < this.mSliders.length; i++) {
            var slider = this.mSliders[i];
            json.sliders[slider.label()] = slider.toJSON();
        }
        return {
            metadata : {
                access : {
                    ro: "",
                    rw: ""
                },
                opaque : {
                    name: profileName
                }
            },
            preferences: json
        };
    }





    /*
     *
     * One of many routing preference sliders
     *
     */
    function Slider(parent, generator, options) {
        this.mOptions   = options || {};
        this.mParent    = parent;
        this.mClass     = "routingPreference";
        if (this.mOptions && this.mOptions.classs) {
            this.mClass = this.mOptions.classs;
        }
        this.mGenerator = new generator(this);
        this.mSliderOptions = this.mGenerator.masterSlideOptions();
        this.mLabel = this.mSliderOptions.label;
    }


    Slider.prototype.render = function(panel) {
        var self     = this;
        return when.all([toolAccordionElemTemplate.promise, sliderTemplate.promise])
        .then(function(templates) {
            panel.append(
                mustache.render(templates[0], {
                    id      : self.mLabel,
                    parent  : "toolsAccordion",
                    class   : self.mClass
                })
            );
            var title = $("#title_"+self.mLabel)
            title.append(
                mustache.render(templates[1], self.mSliderOptions)
            );
            var body = panel.find("#collapse_"+self.mLabel+" .panel-body");
            self.mAccordion = panel.find("#collapse_"+self.mLabel);
            var rendered = self.mGenerator.render(body);
            self.mSlider = $("#"+self.mLabel+"_slider").slider();
            self.mSliderDiv = self.mSlider.parent().parent().find(".slider");
            self.mSliderDiv.css("margin-top", "-13px");
            self.mSlider.on('slideStop', function(ev) {
                self.expand();
                self.mGenerator.fromMasterSlider(self.mSlider.getValue());
                self.mParent.invalidate();
            });

            if (self.mSliderOptions.underConstruction) {
                self.mSliderDiv.find(".slider-selection").css("background", "url(graphics/uc.png)");
                self.mSliderDiv.find(".slider-handle").addClass("hide");
                var sliderTrack = self.mSliderDiv.find(".slider-track");
                sliderTrack.mousedown(function(e) {
                    e.stopPropagation();
                });
                sliderTrack.mouseup(function(e) {
                    e.stopPropagation();
                });
                sliderTrack.attr("data-toggle", "tooltip");
                sliderTrack.attr("data-placement", "bottom");
                sliderTrack.attr("title", "under construction");
            }
            return rendered;
        });
    }

    Slider.prototype.collapse = function() {
        this.mAccordion.collapse('hide');
    }

    Slider.prototype.expand = function() {
        this.mAccordion.collapse('show');
    }

    Slider.prototype.generatePreference = function(rootJson) {
        this.mGenerator.generate(rootJson);
    }

    Slider.prototype.invalidate = function() {
        this.mParent.invalidate();
    }

    Slider.prototype.label = function() {
        return this.mLabel;
    }

    Slider.prototype.fromJSON = function(json) {
        var value = json ? json.masterSlider : this.mSliderOptions.value;
        this.mSlider.setValue(value);
        if (typeof this.mGenerator.fromJSON != "undefined") {
            this.mGenerator.fromJSON(json ? json.generator : undefined);
        }
    }

    Slider.prototype.toJSON = function() {
        var result = {
            masterSlider: this.mSlider.getValue()
        };
        if (typeof this.mGenerator.toJSON != "undefined") {
            result["generator"] = this.mGenerator.toJSON();
        }
        return result;
    }

    return {
        RoutingPreference: RoutingPreference
    }

})();

module.exports = routingPreference;