/**
 * scripts/ui/journeyItem.js
 *
 * Base class for Journey Items
 */

'use strict';

var when          = require("when");
var mustache      = require("mustache");
var map           = require("./map.js");

var journeyItem = (function() {

    var templates = when.defer();
    $.get("templates/journey.template.html", function(template, textStatus, jqXhr) {
        templates.resolve({
            Journey: $(template).filter("#journey_tmpl").html(),
            Route  : $(template).filter("#route_tmpl").html(),
            POI    : $(template).filter("#poi_tmpl").html()
        });
    });

    var journeyItemId = 0;
    function JourneyItem(parent, panelDiv) {
        this.mParent   = parent;
        this.mPanelDiv = panelDiv;
        this.mId       = "journeyItemId_"+journeyItemId++;
        this.mParams   = {
            givenName      : undefined,
            description    : ""
        }
    }

    JourneyItem.prototype.name = function() {
        return this.mParams.givenName;
    }

    JourneyItem.prototype.delete = function() {
        $( "#"+this.mId).remove();
        if (this.mParent) {
            this.mParent.deleteItem(this);
        }
    }

    JourneyItem.prototype.show = function() {
        var self = this;
        var rendered = true;
        if (!this.mDivs) {
            rendered = this.render();
        }
        return when(rendered)
        .then(function() {
            self.mDivs.collapse.collapse("show");
        });
    }

    JourneyItem.prototype.isShown = function() {
        if (this.mDivs) {
            var openPanel = this.mDivs.collapsiblePanel.find(".collapse.in");
            return openPanel.length == 1;
        }
        return false;
    }

    JourneyItem.prototype.hide = function() {
        this.mDivs.collapse.collapse("hide");
    }

    JourneyItem.prototype.centre = function() {
        map.leaflet.fitBounds(
            this.getBounds(),
            {
                animate : true
            }
        );
    }

    JourneyItem.prototype.invalidate = function() {
        var self = this;
        if (!this.mInvalidatePosted) {
            this.mInvalidatePosted = this.render()
            .then(function() {
                self.doInvalidate();
                delete self["mInvalidatePosted"];
                self.mParent.invalidate();
            });
        }
        return this.mInvalidatePosted;
    }

    JourneyItem.prototype.doInvalidate = function() {
        if (!this.mParams.givenName) {
            this.mDivs.name.text(this.name());
        }
    }

    JourneyItem.prototype.render = function(templateName, templateParams) {
        if (this.mDivs) {
            return when(true);
        }
        templateParams = templateParams || {};
        var self = this;
        return when(templates.promise)
        .then(function(templates) {
            templateParams["id"]          = self.mId;
            templateParams["description"] = self.mParams.description || "description";
            templateParams["parent"]      = "journey";
            templateParams["name"]        = self.name();

            self.mPanelDiv.append(mustache.render(templates[templateName], templateParams));
            var collapsiblePanel = self.mPanelDiv.find("#"+self.mId);
            self.mDivs = {
                collapsiblePanel    : collapsiblePanel,
                name                : collapsiblePanel.find(".panelHeading"),
                description         : collapsiblePanel.find(".description"),
                collapse            : collapsiblePanel.find(".collapse"),
                binIcon             : collapsiblePanel.find("#binIcon"),
                centreIcon          : collapsiblePanel.find("#centreIcon"),
                busyIcon            : collapsiblePanel.find("#busyIcon")
            }
            self.mDivs.description.focus(function() {
                if (!self.mParams.description) {
                    self.mDivs.description.text("");
                    self.mDivs.description.removeClass("placeHolderText");
                }
            });
            self.mDivs.description.on("blur keyup paste input", function() {
                self.mParams.description = self.mDivs.description.text();
            });
            collapsiblePanel.on("show.bs.collapse", function () {
                //for some reason, we need to programmatically enforce single open panel
                $("#journey .collapse.in").collapse("hide");
            });
            $('[data-toggle="tooltip"]').tooltip();
            self.mDivs.binIcon.click(function() {
                self.delete();
            });
            self.mDivs.centreIcon.click(function() {
                self.centre();
            });
            self.mDivs.name.blur(function() {
                if (self.name() != $(this).text()) {
                    //something got editted
                    if ($(this).text() == "") {
                        self.mParams.givenName = undefined;
                    } else {
                        self.mParams.givenName = $(this).text();
                    }
                    self.invalidate();
                }
            });
            self.show();
        });
    }

    return {
        JourneyItem : JourneyItem,
        templates   : templates.promise
    };

})();

module.exports = journeyItem;