/**
 * scripts/ui/journeys.js
 *
 * This manages the journeys panel
 */

'use strict';

var when         = require("when");
when["sequence"] = require('when/sequence');
var mustache     = require("mustache");

var fileSaver    = require("../external/fileSaver.js");

var zikesApi     = require("../lib/zikesRouter.js");
var utils        = require("../lib/utils.js");
var googleAPI    = require("../lib/google.js");

var map          = require("./map.js");
var Route        = require("./panelRoute.js").Route;
var POI          = require("./poi.js").POI;

var RoutingPreference = require("./routingPreference.js").RoutingPreference;
var journeyTemplates  = require("./journeyItem.js").templates;

var journeys = (function() {

    function Journeys(journeysPanelDiv, journeyPanelDiv) {
        this.mJourneysDiv = journeysPanelDiv;
        var self          = this;
        this.mJourney     = new Journey(journeyPanelDiv);
        this.mJourney.shown(function(shown) {
            if (!shown) {
                if (!self.mShown) {
                    map.expand();
                } else {
                    self.show();
                }
            }
        });
    }

    Journeys.prototype.show = function() {
        var self = this;
        this.mShown = true;
        return when(this.render())
        .then(function() {
            return when.all([
                map.shrink(),
                self.mJourney.hide()
            ]);
        });
    }

    Journeys.prototype.hide = function() {
        this.mShown = false;
        return map.expand();
    }

    Journeys.prototype.current = function() {
        return this.mJourney;
    }

    Journeys.prototype.render = function() {
        function append(journeyMeta, template) {
            self.mJourneysDiv.append(mustache.render(template, {
                id: journeyMeta.id,
                description: journeyMeta.opaque.description || "description",
                name: journeyMeta.opaque.name
            }));
            var journeyDiv = $("#"+journeyMeta.id);

            journeyDiv.find("#open").click(function() {
                zikesApi.loadPlannedJourney(journeyMeta.id)
                .then(function(journeyJSON) {
                    self.mJourney.fromJSON(journeyJSON);
                });
            });
            journeyDiv.find("#delete").click(function() {
                zikesApi.deletePlannedJourney(journeyMeta.id)
                .then(function() {
                    self.show();
                });
            });
        }

        var self = this;
        var now = new Date().getTime();
        if (!this.mLastReloaded || (now-this.mLastReloaded > 10000) ) {
            this.mLastReloaded = now;
            return when.all([zikesApi.plannedJourneys(), journeyTemplates])
            .then(function(results) {
                var journeyMetas = results[0];
                var template = results[1].Journey;
                self.mJourneysDiv.empty();
                var heading = "Your Journeys"
                if (journeyMetas.length == 0) {
                    heading = "You have no saved journeys";
                }
                self.mJourneysDiv.append("<h5>"+heading+"</h5>");
                journeyMetas.map(function(journeyMeta) {
                    append(journeyMeta, template);
                });
            });
        }
    }














    function Journey(panelDiv) {
        function newMetadata() {
            return {
                opaque: {},
                access: {
                    ro: "",
                    rw: ""
                }
            };
        }
        this.mJourneyDiv = panelDiv;
        this.mObservers = {
            shown : []
        };
        this.mItems     = [];
        this.mMetadata  = newMetadata();
        var self        = this;
        this.mRoutingPreferences = new RoutingPreference(
            function() { self.route(); }
        );
        $("#journeyDownload").click(function() {
            self.download();
        });
        $("#journeySave").click(function() {
            self.autoSave();
        });
        $("#journeyClose").click(function() {
            self.mMetadata = newMetadata();
            self.hide();
        });
        this.mJourneyDiv.find("#journeyName").blur(function() {
            if (self.name() != $(this).text()) {
                //something got editted
                if ($(this).text() == "") {
                    self.mMetadata.opaque.givenName = undefined;
                } else {
                    self.mMetadata.opaque.givenName = $(this).text();
                }
                self.invalidate();
            }
        });
    }

    Journey.prototype.forEach = function(what, cb) {
        this.mItems.forEach(function(item) {
            if (item instanceof what) {
                cb(item);
            }
        });
    }

    Journey.prototype.invalidate = function(options) {
        var self = this;
        this.mMetadata.opaque.name = this.name();
        this.mJourneyDiv.find("#journeyName").text(this.mMetadata.opaque.name);
        var distanceMts = 0;
        this.forEach(Route, function(route) {
            distanceMts += route.distanceMts();
        });
        this.mJourneyDiv.find("#journeyDistance").text(utils.mtsToString(distanceMts));
        if (this.mItems.length) {
            if (this.mItems.length == 1) {
                var theOnlyItem = this.mItems[0]
                if (theOnlyItem instanceof Route && theOnlyItem.isPending()) {
                    return;
                }
            }
            this.show();
            if (this.mMetadata.opaque.autosave) {
                if (!(options && options.dontSave)) {
                    this.save();
                }
            } else {
                $("#journeySave").removeClass("hide");
            }
        } else {
            self.hide();
            if (this.mMetadata.opaque.autosave && !(options && options.dontSave)) {
                this.delete();
            }
        }
    }

    Journey.prototype.shown = function(cb) {
        this.mObservers.shown.push(cb);
    }

    Journey.prototype.show = function() {
        var journeyDiv = $("#journeyWrapper");
        if (!journeyDiv.hasClass("hidden")) {
            return when(true);
        }
        if (!("mShowing" in this)) {
            journeyDiv.removeClass("hidden");
            var self = this;
            this.mShowing = map.shrink()
            .then(function() {
                var finished = when.defer();
                journeyDiv.animate({"left" : "0px"}, function() {
                    finished.resolve();
                });
                return when(finished.resolve)
                .then(function() {
                    self.mObservers.shown.forEach(function(observerCb) {
                        observerCb(true);
                    });
                    delete self["mShowing"];
                });
            });
        }
        return this.mShowing;
    }

    Journey.prototype.hide = function() {
        var journeyDiv = $("#journeyWrapper");
        if (journeyDiv.hasClass("hidden")) {
            return when(true);
        }
        if (!("mHiding" in this)) {
            var self = this;
            var animatedAway = when.defer();
            $("#journeyWrapper").animate({"left" : "-300px"}, function() {
                journeyDiv.addClass("hidden");
                animatedAway.resolve();
            });
            this.mHiding = when(animatedAway.promise).then(function() {
                self.mItems.forEach(function(item) {
                    delete item["mParent"] //prevent from invalidating parent
                    item.delete();
                });
                self.mItems = [];
                self.mObservers.shown.forEach(function(observerCb) {
                    observerCb(false);
                });
                delete self["mHiding"];
            });
        }
        return this.mHiding;
    }

    Journey.prototype.name = function() {
        if (this.mMetadata.opaque.givenName) {
            return this.mMetadata.opaque.givenName;
        }
        var from, to;
        this.forEach(Route, function(route) {
            if (!route.isPending()) {
                if (!from) {
                    from = route.from().geocode;
                }
                to = route.to().geocode;
            }
        });
        if (from && to) {
            var fromTo = googleAPI.geocodeHumanReadable([from,to]);
            return fromTo[0] + " -> " + fromTo[1];
        }
        return "New Journey";
    }

    Journey.prototype.pendingRoute = function() {
        var lastRoute;
        this.forEach(Route, function(route) {lastRoute = route} );
        if (!lastRoute || !lastRoute.isPending()) {
            lastRoute = new Route(this, this.mJourneyDiv);
            this.mItems.push(lastRoute);
        }
        return lastRoute;
    }

    Journey.prototype.newPOI = function(latlng) {
        this.mItems.push(
            new POI(this, this.mJourneyDiv, latlng)
        );
    }

    Journey.prototype.route = function(milestones) {
        if (milestones) {
            //this is request from the UI towards the server
            return zikesApi.requestRoute(
                milestones,
                this.mRoutingPreferences.generatePreference()
            );
        } else {
            //this is request towards the UI - to reroute as circumstances changed
            this.forEach(Route, function(route) {
                if (route.isShown()) {
                    route.route();
                }
            });
        }
    }

    Journey.prototype.download = function() {
        if (this.mItems.length) {
            var result = '<?xml version="1.0" encoding="UTF-8"?>\n'
            result    += '<gpx xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd" version="1.1" creator="BTT for Qt" xmlns="http://www.topografix.com/GPX/1/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1" xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3">\n';
            result    += '<metadata><name>'+this.name()+'</name></metadata>\n';
            this.mItems.forEach(function(item) {
                result += item.toGpx();
            });
            result += "</gpx>";
            var blob = new Blob([result], {type: "text/xml"});
            fileSaver.saveAs(blob, this.name()+".gpx");
        }
    }

    Journey.prototype.toJSON = function() {
        return {
            metadata: this.mMetadata,
            journey: {
                items: this.mItems.map(function(item) {
                    if (item instanceof Route) {
                        return { route : item.toJSON() };
                    } else if (item instanceof POI) {
                        return { poi : item.toJSON() };
                    }
                    return { item : item.toJSON() };
                })
            }
        }
    }

    Journey.prototype.fromJSON = function(json) {
        var self = this;
        this.mMetadata = json.metadata;
        if (this.mMetadata.opaque.autosave) {
            $("#journeySave").addClass("hide");
        }
        this.mItems.forEach(function(item) {
            item.delete();
        });
        utils.assert(this.mItems.length == 0);
        this.invalidate = function() {} //ignore any invalidates flowing during deserialisation
        when.sequence(
            json.journey.items.map(function(itemJSON) {
                return function() {
                    var item = undefined;
                    if ("route" in itemJSON) {
                        item = new Route(self, self.mJourneyDiv);
                        itemJSON = itemJSON["route"];
                    } else if ("poi" in itemJSON) {
                        item = new POI(self, self.mJourneyDiv);
                        itemJSON = itemJSON["poi"];
                    } else {
                        assert(false, "unknown journey item type: "+ JSON.stringify(itemJSON))
                        return;
                    }

                    self.mItems.push(item);
                    return item.fromJSON(itemJSON);
                }
            })
        ).then(function() {
            self.show();
        }).then(function() {
            self.centre();
            delete self["invalidate"];
            self.invalidate({dontSave:true});
        });
    }

    Journey.prototype.centre = function() {
        if (this.mItems.length) {
            var bounds = this.mItems.reduce(function(cum, item) {
                return cum.extend(item.getBounds())
            }, this.mItems[0].getBounds());

            map.leaflet.fitBounds(
                bounds,
                {
                    animate : true
                }
            );
        }
    }

    Journey.prototype.save = function() {
        var self = this;
        zikesApi.savePlannedJourney(
            this.toJSON()
        ).then(function(metadata) {
            $("#journeySave").addClass("hide");
            self.mMetadata = metadata;
        });
    }

    Journey.prototype.delete = function() {
        zikesApi.deletePlannedJourney(
            this.mMetadata.id
        );
        this.hide();
    }

    Journey.prototype.autoSave = function() {
        this.mMetadata.opaque.autosave = true;
        this.save();
    }

    Journey.prototype.deleteItem = function(item) {
        for (var i = 0; i < this.mItems.length; i++) {
            if (this.mItems[i] == item) {
                this.mItems.splice(i,1);
                break;
            }
        }
        this.invalidate();
    }

    return {
        Journeys: Journeys,
        Journey : Journey
    }
})();

module.exports = journeys;