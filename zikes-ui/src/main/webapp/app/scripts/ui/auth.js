/**
 * scripts/ui/auth.js
 *
 * Handles authentication (with ui)
 */

'use strict';

var when     = require("when");
var hello    = require("hellojs");
var mustache = require("mustache");

var auth = (function() {
    var GOOGLE_CLIENT_ID = "787067721578-396usv4alu7u9rjfnf0mi2pa6m7r8jps.apps.googleusercontent.com";
    var FACEBOOK_CLIENT_ID = "955510821203918";

    var networks = {
        facebook: FACEBOOK_CLIENT_ID,
        google: GOOGLE_CLIENT_ID
    }

    hello.init(networks, {redirect_uri: 'auth_redirect.html'});

    var authDialogTemplate = when.defer();
    $.get("templates/ui.template.html", function(template, textStatus, jqXhr) {
        var html = $(template).filter("#authDialogTemplate_tmpl").html()
        authDialogTemplate.resolve(html);
    });

    function getAuthenticatedNetwork() {
        var helloStorage = JSON.parse(
            window.localStorage.getItem("hello")
        );
        if (helloStorage) {
            for (var networkName in networks) {
                var authenticatedNetwork = helloStorage[networkName];
                if (authenticatedNetwork && authenticatedNetwork.access_token) {
                    return networkName;
                }
            }
        }
    }

    var userDetailsRetrieved = undefined;
    function userDetails(details) {
        if (!userDetailsRetrieved) {
            userDetailsRetrieved = when.defer();
        }

        if (details) {
            userDetailsRetrieved.resolve(details)
        }
        return userDetailsRetrieved.promise;
    }


    function doLogin(network, force) {
        var currentSession = hello(network).getAuthResponse();
        var currentTime = (new Date()).getTime() / 1000;
        if (currentSession && currentSession.access_token && (currentTime - currentSession.expires < -100) && userDetails().inspect().state == "fulfilled") {
            return userDetails();
        }
        var result = when.defer();
        hello(network)
        .login({
            display: "popup",
            force  : force
        })
        .then(function(response) {
                when(userDetails())
                .then(function(details) {
                    for (var detailName in details) { response[detailName] = details[detailName]; }
                    result.resolve(response);
                })
                .otherwise(function(e) {
                    result.reject(e);
                });
            }, function(e) {
                console.error("error loggin in", e.error);
                result.reject(e);
            }
        );
        return result.promise;
    }

    function login(prompt) {
        var knownNetwork = getAuthenticatedNetwork();
        if (knownNetwork) {
            return doLogin(knownNetwork, false);
        } else if (prompt) {
            return when(authDialogTemplate.promise)
            .then(function(template) {
                var result = when.defer();
                function settle(promise) {
                    when(promise)
                    .then(function(value) {
                        result.resolve(value);
                    })
                    .otherwise(function(e) {
                        result.reject(e);
                    });
                }

                $(document.body).append(
                    mustache.render(
                        template, {
                            prompt:prompt
                        }
                    )
                );
                var modal = $("#authModal").modal({"backdrop" : "static"});
                modal.find("#google").click(function() {
                    modal.modal("hide");
                    settle(doLogin("google", true));
                });
                modal.find("#facebook").click(function() {
                    modal.modal("hide");
                    settle(doLogin("facebook", true));
                });
                modal.on('hidden.bs.modal', function (e) {
                    modal.remove();
                });
                return result.promise;
            });
        }
    }

    var subscribers = {
        login:  [],
        logout: []
    }
    hello.on("auth.login", function(r) {
        // Get Profile
        hello( r.network ).api( '/me' ).then( function(p) {
            p.network = r.network;
            p.authResponse = r.authResponse;
            var userNavItem = $("#userNavItem");
            userNavItem.find(".dropdown-toggle").html(
                '<img src="'+ p.thumbnail + '" width="30px" height="30px" style="border-width:1px;border-style:inset;"/><span style="padding-left:4px">'+p.name+'</span><b class="caret"></b>'
            );
            $("#loginNavItem").addClass("hide");
            userNavItem.removeClass("hide");
            userDetails(p);
            subscribers.login.forEach(function(subscriber) {
                subscriber(p);
            });
        });
    });

    hello.on("auth.logout", function(r) {
        $("#loginNavItem").removeClass("hide");
        $("#userNavItem").addClass("hide");
        userDetailsRetrieved = undefined;
        subscribers.logout.forEach(
            function(subscriber) {subscriber();}
        );
    });

    function logout() {
        hello.logout(
            getAuthenticatedNetwork()
        );
    }

    function on(what, cb) {
        subscribers[what].push(cb);
    }

    $("#loginNavItem").click(function() {
        login("Choose Network");
    });

    $("#logoutNavItem").click(function() {
        logout();
    });

    return {
        login : login,
        logout: logout,
        on    : on
    };
})();

module.exports = auth;