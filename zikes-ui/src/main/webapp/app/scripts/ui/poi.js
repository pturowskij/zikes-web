/**
 * scripts/ui/poi.js
 *
 * This manages the route item in the Journey panel
 */

'use strict';

var when          = require("when");
when["sequence"]  = require('when/sequence');
var googleAPI     = require("../lib/google.js");
var map           = require("./map.js");
var messages      = require("./messages.js");

var journeyItem   = require("./journeyItem.js");
var JourneyItem   = journeyItem.JourneyItem;
var journeyTemplates = journeyItem.templates;

var poi = (function() {

    function POI(parent, panelDiv, latlng) {
        JourneyItem.call(this, parent, panelDiv);
        this.latlng(latlng);
    }
    POI.prototype = new JourneyItem();
    POI.prototype.constructor = POI;


    POI.prototype.name = function() {
        var result = JourneyItem.prototype.name.call(this);
        if (!result) {
            if (this.mParams.geocode) {
                result = googleAPI.geocodeHumanReadable(
                    this.mParams.geocode, 3
                );
            } else {
                result = "New Point of Interest";
            }
        }
        return result;
    }

    POI.prototype.doInvalidate = function() {
        if (!this.mParams.latlng) {
            this.delete();
            return;
        }
        JourneyItem.prototype.doInvalidate.call(this);
    }

    POI.prototype.toJSON = function() {
        return this.mParams;
    }

    POI.prototype.fromJSON = function(json) {
        this.mParams = json;
        this.show = function() { //prevent expanding accordion when deserialising
            delete self["show"];
            return when(true);
        }
        return this.latlng(this.mParams.latlng);
    }

    POI.prototype.latlng = function(latlng) {
        var self = this;
        function geocode() {
            return googleAPI.reverseGeocodeFind(self.mParams.latlng)
            .then(function(geocode) {
                self.mParams.geocode = geocode;
                return self.invalidate();
            });
        }

        var self = this;
        if (latlng) {
            if (this.mLmMarker) {
                map.leaflet.removeLayer(this.mLmMarker);
            }
            if (latlng instanceof L.LatLng) {
                latlng = [latlng.lat, latlng.lng]
            }
            this.mParams.latlng = latlng;
            this.mLmMarker = L.marker(this.mParams.latlng, {
                draggable : true
            }).addTo(map.leaflet);
            this.mLmMarker.on("click", function(e) {
                var popup;
                function deleteme() {
                    self.delete();
                    map.leaflet.removeLayer(popup);
                }
                var c = document.createElement('div');
                c.onclick=deleteme;
                c.innerHTML="<a href=\"#\">Delete</a>";
                popup = L.popup()
                    .setLatLng(e.latlng)
                    .setContent(c)
                    .openOn(map.leaflet);
            });
            this.mLmMarker.on("dragend", function() {
                self.mParams.geocode = undefined;
                var latlng = self.mLmMarker.getLatLng();
                self.mParams.latlng = [latlng.lat, latlng.lng];
                geocode();
            });
            return geocode();
        }
        return this.mParams.latlng;
    }

    POI.prototype.getBounds = function() {
        return  L.latLngBounds(
            L.latLng(this.mParams.latlng[0], this.mParams.latlng[1]),
            L.latLng(this.mParams.latlng[0], this.mParams.latlng[1])
        );
    }

    POI.prototype.toGpx = function() {
        var result = '<rte name="'+this.name()+'">\n';
        result += '  '+this.mRouteOnMap.gpxRtePts() + '\n</rte>';
        return result;
    }

    POI.prototype.render = function() {
        return JourneyItem.prototype.render.call(
            this,
            "POI"
        )
    }

    POI.prototype.delete = function() {
        JourneyItem.prototype.delete.call(this);
        if (this.mLmMarker) {
            map.leaflet.removeLayer(this.mLmMarker);
        }
    }

    return { POI : POI };

})();

module.exports = poi;