/**
 * scripts/ui/mapRoute.js
 *
 * This draws the route on the map and takes care of the interactions
 * with it.
 */

'use strict';

var when       = require("when");
var utils      = require("../lib/utils.js");
var geometry   = require("../lib/geometry.js");
var map        = require("./map.js");
var ctxMenu    = require("./contextMenu.js");
var streetView = require("./streetView.js");

var route = (function() {

    var KRouteStyle = {
        weight: 5,
        opacity: 0.5,
        smoothFactor: 1,
        color : "#0003F0"
    };

    var KRouteStyleHighlight = {
        weight: 7,
        opacity: 0.5,
        smoothFactor: 1,
        color : "#8003F0"
    };

    function geoComparableDistance(from, to) {
        return Math.pow(from.lat - to.lat, 2) + Math.pow(from.lng - to.lng, 2);
    }


    function Route(parent, jsonDocument) {
        this.mHead          = undefined;
        this.mHasFrom       = false;
        this.mRouteStyle    = KRouteStyle;
        this.mParent        = parent;
        var prev            = undefined
        if (jsonDocument) {
            for (var i = 0; i < jsonDocument.sections.length; i++) {
                var section = new Section(this, {
                    prev     : prev,
                    opacity  : 0.5,
                    iconClass: 'toFromIcon',
                    latlng   : jsonDocument.sections[i].latlng
                });
                if (!this.mHead) {
                    this.mHead = section;
                }
                if (prev) {
                    prev.mNext = section;
                }
                prev = section;
            }
        }
    }

    Route.prototype.invalidate = function(force) {
        var self = this;
        var tail = this.mHead;
        var head = undefined;
        var routeRequestCoordinates = [];
        var routesPrepared  = [];

        while (tail) {
            //Some sections may need (re)routing (they were dragged) and if they do,
            //we need to keep addting their new coordinates to routeRequestCoordinates
            //for as long as we encounter stale nodes and thus compose a single
            //consistent from,[via...],to request.
            if (tail.needsRouting() || force) {
                if (!head) {
                    //remember the first section (which will receive the route request result)
                    head = tail;
                }
                routeRequestCoordinates.push(WayMarkerBase.prototype.toJSON.call(tail));
            } else if (routeRequestCoordinates.length) {
                //If the stale nodes are interleaved with not stale ones, we need to issue
                //more than one routing request (for detached sections).
                routeRequestCoordinates.push(WayMarkerBase.prototype.toJSON.call(tail));
                routesPrepared.push(
                    head.prepareRoute(this.mParent.route(routeRequestCoordinates), tail)
                );
                routeRequestCoordinates = [];
            }
            tail = tail.mNext;
        }
        //and the last request
        if (routeRequestCoordinates.length > 1) {
            routesPrepared.push(
                head.prepareRoute(this.mParent.route(routeRequestCoordinates), this.mHead.tail())
            );
        }

        if (routesPrepared.length > 0 || (!this.isPending() && !this.mHead.mDrawnRoute)) {
            return when.all(routesPrepared)
            .then(function() {
                self.draw();
                return self.mParent.invalidate();
            });
        }
    };

    Route.prototype.head = function(newHead)
    {
        if (!newHead) {
            //getter
            return this.mHead;
        }
        //the scenario for this setter is now only one.
        //the former 'from' has been removed by user
        //action. If this changes, maybe the conclussion
        //below will not be correct
        this.mHasFrom = false;
        this.mHead = newHead;
        return this.mHead;
    };

    /**
     * Setter and getter for this.from.
     * @latlng  - array of floats carrying the new coordinates for this.from.
     *            when undefined, the method behaves as a getter.
     * @returns - true - if the given latlng differs from current (false otherwise)
     */
    Route.prototype.from = function(latlng)
    {
        if (!latlng) {
            //getter
            if (this.mHead) {
                if (this.mHead != this.mHead.tail() || this.mHasFrom) {
                    return this.mHead.getLatLng();
                }
            }
            return undefined;
        }
        //setter...
        var currentFrom = this.from();
        if (currentFrom) {
            if (currentFrom == latlng) {
                return false;
            }
            this.mHead.setLatLng(latlng);
        }
        else {
            this.mHead = new Section(this, {
                next     : this.mHead,
                opacity  : 0.5,
                iconClass: 'toFromIcon',
                latlng   : latlng
            });
        }
        this.mHasFrom = true;
        return true;
    };

    /**
     * Setter and getter for this.to.
     * @latlng  - array of floats carrying the new coordinates for this.to.
     *            when undefined, the method behaves as a getter.
     * @returns - true - if the given latlng differs from current (false otherwise)
     */
    Route.prototype.to = function(latlng)
    {
        if (!latlng) {
            //getter
            if (this.mHead) {
                if (this.mHead != this.mHead.tail() || !this.mHasFrom) {
                    return this.mHead.tail().getLatLng();
                }
            }
            return undefined;
        }
        //setter...
        var currentTo = this.to();
        if (currentTo) {
            if (currentTo == latlng) {
                return false;
            }
            this.mHead.tail().setLatLng(latlng);
        } else {
            var to = new Section(this, {
                opacity  : 0.5,
                iconClass: 'toFromIcon',
                latlng   : latlng
            });
            if (!this.mHead) {
                this.mHead = to;
            } else {
                to.mPrev = this.mHead.tail();
                this.mHead.tail().mNext = to;
            }
        }
        return true;
    };

    Route.prototype.isPending = function(latlng)
    {
        return !(this.mHead && this.mHead.mNext);
    };

    Route.prototype.toJSON = function()
    {
        var result = [];
        this.forEachSection(function(section) {
            result.push(
                section.toJSON()
            );
        });
        return result;
    };

    Route.prototype.fromJSON = function(sectionsJSON)
    {
        this.delete();
        for (var i = 0; i < sectionsJSON.length; i++) {
            var section = new Section(this, {
                opacity  : 0.5,
                iconClass: 'toFromIcon',
                latlng   : sectionsJSON[i].from
            });
            if (!this.mHead) {
                this.mHead = section
            } else {
                section.mPrev = this.mHead.tail();
                section.mPrev.mNext = section;
                section.mPrev.fromJSON(sectionsJSON[i-1].waypoints);
            }
        }
        return this.invalidate();
    };

    Route.prototype.getDistanceMts = function()
    {
        var result = 0;
        this.forEachSection(function(section) {
            result += section.mDistanceMts;
        });
        return result;
    };

    Route.prototype.getProfile = function()
    {
        var result  = [];
        var current = this.mHead;
        while (current && current.mWaypoints) {
            if (!current.mProfile) {
                return //abort, unreliable profile
            }
            result = result.concat(current.mProfile);
            current = current.mNext;
        }
        return result.length ? result : undefined;
    };

    Route.prototype.getBounds = function()
    {
        return this.forEachSection(function(section, bounds) {
            if (!bounds) {
                return section.getBounds();
            }
            return bounds.extend(section.getBounds());
        });
    };

    Route.prototype.highlight = function(highlight) {
        this.forEachSection(function(section) {
            section.highlight(highlight);
        });
        if (typeof highlight === 'number' || highlight == false) {
            this.mParent.highlight(highlight);
        }
    };

    Route.prototype.draw = function(reroute)
    {
        this.forEachSection(function(section) {
            section.draw();
        });
    };

    Route.prototype.delete = function(reroute)
    {
        this.forEachSection(function(section) {
            section.delete();
        });
        this.mHead    = undefined;
        this.mHasFrom = false;
    };

    Route.prototype.forEachSection = function(f, initialResult)
    {
        var result = initialResult;
        var current = this.mHead;
        while (current) {
            result = f(current, result);
            current = current.mNext;
        }
        return result;
    };

    Route.prototype.gpxRtePts = function() {
        var result = "";
        this.forEachSection(function(section) {
            if (section.mWaypoints) {
                var waypoint;
                var ele = "";
                for (var i = 0; i < section.mWaypoints.length; i++) {
                    waypoint = section.mWaypoints[i];
                    if (waypoint.decoration && waypoint.decoration.eleMts != undefined) {
                        ele = '<ele>'+waypoint.decoration.eleMts+'</ele>';
                    }
                    result += '<rtept lat="'+waypoint.lat+'" lon="'+waypoint.lng+'">'+ele+'</rtept>\n';
                    ele = "";
                }
            }
        });
        return result;
    }

    function mts2userReadable(mts) {
        mts = 1*mts;
        if (mts < 1000) {
            return Math.round(mts)+"m";
        } else if (mts < 2000) {
            return Math.round(mts/100)/10+"km";
        } else {
            return Math.round(mts/1000)+"km";
        }
    }






    /*
     *
     * Basic functionality of a waymarker (a to/from/via point)
     *
     */
    function WayMarkerBase(parent, params) {
        if (!params) {
            return;
        }
        this.mParent = parent;
        this.mLmMarker    = L.marker(params.latlng, {
            icon: L.divIcon({
                className: params.iconClass
            }),
            opacity   : params.opacity,
            draggable : true
        }).addTo(map.leaflet);

        var self = this;
        this.mLmMarker.on("dragstart", function() {
            self.onDragStart();
        });
        this.mLmMarker.on("dragend", function() {
            self.onDragEnd();
        });
    }


    WayMarkerBase.prototype.delete = function()
    {
        map.leaflet.removeLayer(this.mLmMarker);
    };

    WayMarkerBase.prototype.onDragEnd = function()
    {
        this.mBeingDragged = false;
    };

    WayMarkerBase.prototype.onDragStart = function()
    {
        this.mPrevLatLng = this.getLatLng();
        this.mBeingDragged = true;
    };

    WayMarkerBase.prototype.unstale = function()
    {
        this.mPrevLatLng = undefined;
    };

    WayMarkerBase.prototype.setOpacity = function(opacity)
    {
        this.mLmMarker.setOpacity(opacity);
    };

    WayMarkerBase.prototype.setLatLng = function(latlng, onRoute) {
        this.mPrevLatLng = this.getLatLng();
        this.mLmMarker.setLatLng(latlng);
    };

    WayMarkerBase.prototype.getLatLng = function(latlng) {
        return this.mLmMarker.getLatLng();
    };

    WayMarkerBase.prototype.toJSON = function() {
        return [this.mLmMarker.getLatLng().lat, this.mLmMarker.getLatLng().lng];
    };

    WayMarkerBase.prototype.hasMoved = function()
    {
        return !!this.mPrevLatLng;
    };

    WayMarkerBase.prototype.isTemp = function()
    {
        return !((this.mNext && this.mNext.mPrev == this) ||
                 (this.mPrev && this.mPrev.mNext == this));
    };












    /*
     * A Section is a node in a doubly-linked list representing a route with all its
     * via points. A Section object is therefore a route's start, end or one of its via points.
     * Section's mWaypoints is an array of latlng coordinates spanning between 'this' and mNext.
     * The tail (closing) section has no mWaypoints.
     */
    function Section(parentRoute, params) {
        WayMarkerBase.call(this, parentRoute, params);
        this.mPrev          = params.prev;
        this.mNext          = params.next;
        this.mWaypoints     = undefined;
        this.mProfile       = undefined;
        this.mDistanceMts   = 0;
        var self = this;
        this.mLmMarker.on("click", function(e) {
            var menu = ctxMenu(e.latlng).default().streetView({delete:true});
            if (!self.mNext) {
                //last
                menu.from({display: "Continue From"});
            }
            menu.separator();
            if (!self.isTemp()) {
                menu.delete({action: function() {
                    self.delete();
                }});
            }
            if (!streetView.shown()) {
                menu = menu.streetView({
                    route:new WaypointIterator(self)
                })
            }
            menu.show();
        });
        this.mLmMarker.on('mouseover', function(e) {
            setTimeout(function() {
                if (self.mLmMarker._map) {
                    streetView.update(
                        new WaypointIterator(self)
                    );
                }
            }, 300);
        });
    }
    Section.prototype = new WayMarkerBase();
    Section.prototype.constructor = Section;

    Section.prototype.needsRouting = function()
    {
        return this.mNext &&
               (WayMarkerBase.prototype.hasMoved.call(this) ||
                WayMarkerBase.prototype.hasMoved.call(this.mNext) || //either end has moved
               !this.mWaypoints); //or there is no waypoints
    };

    Section.prototype.tail = function()
    {
        if (this.mNext) {
            return this.mNext.tail();
        }
        return this;
    };

    Section.prototype.toJSON = function() {
        var result = {
            from : WayMarkerBase.prototype.toJSON.call(this),
        }
        if (this.mWaypoints) {
            result["waypoints"] = this.mWaypoints.map(function(elem) {
                var result = [elem.lat, elem.lng];
                if (elem.decoration && elem.decoration.eleMts != undefined) {
                    result.push(elem.decoration.eleMts);
                    if (elem.decoration.highway != undefined) {
                        result.push(elem.decoration.highway);
                    }
                }
                return result;
            });
        }
        return result;
    };

    Section.prototype.onDragEnd = function()
    {
        WayMarkerBase.prototype.onDragEnd.call(this);
        if (this.mPrev) {
            this.mPrev.mNext = this;
        }
        if (this.mNext) {
            this.mNext.mPrev = this;
        }
        this.mParent.invalidate();
    };

    /*
     * We're having a deficient server, which doesn't give us all the
     * elevations it has. We need to keep this workaround in place
     * until it's fixed.
     */
    function sanitiseProfile(profile) {
        function nextEle(idx) {
            var distanceMts = 0;
            for (var j = idx; j < profile.length; j++) {
                var node = profile[i];
                distanceMts += node.fromPrevJntnMts;
                if (!(isNaN(node.eleMts) || node.eleMts == null)) {
                    return [distanceMts, node.eleMts]
                }
            }
        }

        var result              = [];
        var prevJunction        = undefined;
        var unknownEleNodes     = 0;
        var originalNodesNo     = profile.length;
        var firstIdxWithEle     = undefined;
        var lastIdxWithEle      = undefined;
        var additionalDistance  = 0;
        for (var i = 0; i < profile.length; i++) {
            var node = profile[i];
            if (isNaN(node.eleMts) || node.eleMts == null) {
                unknownEleNodes++;
                var next = nextEle(i);
                if (!(prevJunction && next)) {
                    additionalDistance += node.fromPrevJntnMts;
                    continue;
                }
                var dEle    = next[1]-prevJunction.eleMts;
                var slope   = dEle/next[0];
                node.eleMts = prevJunction.eleMts+(slope*dEle) | 0;
            } else {
                if (firstIdxWithEle == undefined) {
                    firstIdxWithEle = i;
                }
                lastIdxWithEle = i;
            }
            node.fromPrevJntnMts += additionalDistance;
            result.push(node);
            additionalDistance = 0;
            prevJunction = node;
        }
        if (1.0*profile.length/originalNodesNo < 1.0/3 ||
            firstIdxWithEle > 4 || lastIdxWithEle < profile.length-5) {
            return undefined;
        }
        return result;
    }

    /*
     * Distribute the json representation of a route's fragment (routing result or product of earlier toJSON) into the
     * sections following 'this'. In a general case, a route request has been issued between two existing
     * sections and number intermediates (optional via points). Now the result has come as single array of latlng
     * coordinates and, should via points been specified in the request, we need to find them between
     * this and 'end'.
     */
    Section.prototype.fromJSON = function(waypointsJSON, end) {
        end = end || this.mNext;

        //Create a sub-list of placeholder nodes (sections) spanning between 'this' and 'end'.
        //start from one behind this as sections will be patched with a tail later (optimisation note:
        //if there are only two sections in the picture, no point in finding intermediate matches)
        var current = this.mNext;
        var sections = [];
        while (current != end) {
            utils.assert(current, "Unable to find the alleged end");
            sections.push({
                nextSection: current
            });
            current = current.mNext;
        }

        //Now scan the route request result and, whilst calculating the rolling distance and route profile, find best section
        //matches, i.e.: given a waypoint, calculate its distance to all the candiate sections and, within each section, store
        //the index of the nearest waypoint. The result should be:
        //All route's sections:
        //S          ->            S            ->           S              ->             S          ->          S
        //subset sections[] =     this                       x                            end
        //                         |                         |                             |
        //                         v                         V                             V
        //route req result  =      .........................................................
        var routeProfile            = [];
        var prevllWayPoint          = undefined;
        var distanceFromStartMts    = 0;
        var fromPrevJntnMts         = 0;
        for (var i = 0; i < waypointsJSON.length; i++) {
            var waypoint   = waypointsJSON[i];
            var llwaypoint = new L.LatLng(waypoint[0], waypoint[1]);
            if (prevllWayPoint) {
                var distanceFromPrev         = prevllWayPoint.distanceTo(llwaypoint) | 0;
                distanceFromStartMts        += distanceFromPrev;
                fromPrevJntnMts += distanceFromPrev;
            }
            var decoration = {
                distanceFromStartMts: distanceFromStartMts
            }

            utils.assert(waypoint.length != 3 || i < waypointsJSON.length)
            if (waypoint.length > 2) {
                //this is a junction with extra information (altitude and highway type)
                decoration["fromPrevJntnMts"] = fromPrevJntnMts;
                decoration["eleMts"]          = waypoint[2];
                if (waypoint.length > 3) {
                    decoration["highway"]     = waypoint[3];
                }
                routeProfile.push(decoration);
                fromPrevJntnMts = 0;
            }
            llwaypoint["decoration"] = decoration;
            waypointsJSON[i] = llwaypoint;
            for (var j = 0; j < sections.length; j++) {
                var section = sections[j];
                var distanceToWaymarker = geoComparableDistance(llwaypoint, section.nextSection.getLatLng());
                if (section.distanceToNearest == undefined || section.distanceToNearest > distanceToWaymarker) {
                    section.distanceToNearest       = distanceToWaymarker;
                    section.distanceFromStartMts    = distanceFromStartMts;
                    section.nearestWaypointIdx      = i;
                    section.routeProfileStartIdx    = routeProfile.length-1;
                }
            }
            prevllWayPoint = llwaypoint;
        }
        sections.push({
            nearestWaypointIdx   : waypointsJSON.length-1,
            distanceFromStartMts : distanceFromStartMts,
            routeProfileStartIdx : routeProfile.length-1
        }); //for the tail

        //finally: slice the route request result to distribute the waypoints amongst the sections[]
        //make sure each stores the adequate distance as well as route profile.
        var prevWaypointIdx = 0;
        var prevProfileIdx  = 0;
        var distanceBefore  = 0;
        for (var j = 0; j < sections.length; j++) {
            var section = sections[j];
            section.waypoints = waypointsJSON.slice(prevWaypointIdx, section.nearestWaypointIdx+1);
            section.profile   = sanitiseProfile(
                routeProfile.slice(prevProfileIdx, section.routeProfileStartIdx+1)
            );
            distanceBefore += (section.distanceMts = section.distanceFromStartMts - distanceBefore);
            prevWaypointIdx = section.nearestWaypointIdx;
            prevProfileIdx  = section.routeProfileStartIdx;

            routeProfile[prevProfileIdx] = { //this one needs overwriting as we can't manipulate the profile of the previous section
                highway         : routeProfile[prevProfileIdx].highway,
                distanceFromStartMts: routeProfile[prevProfileIdx].distanceFromStartMts,
                fromPrevJntnMts : 0 //adjacent profiles will share nodes, but the one looking forward must have fromPrevJntnMts == 0
            }
        }
        this.setSections(sections);
    };

    Section.prototype.setSections = function(sections)
    {
        if (sections.length) {
            this.mDistanceMts = sections[0].distanceMts;
            this.mWaypoints   = sections[0].waypoints;
            this.mProfile     = sections[0].profile;
            this.mNext.setSections(sections.splice(1))
        }
    };

    Section.prototype.prepareRoute = function(routesPromise, end)
    {
        var self = this;
        return when(routesPromise)
        .then(function(route) {
            self.fromJSON(route.waypoints, end);
        });
    };

    Section.prototype.projectOnto = function(point)
    {
        if (point instanceof L.Point) {
            return this.projectPixOnto(point);
        } else if (point instanceof L.LatLng) {
            return this.projectLatLngOnto(point);
        }
    };

    Section.prototype.projectPixOnto = function(pixPoint)
    {
        function projectOnto(points, nearestSegment, idxStart, idxEnd) {
            nearestSegment = nearestSegment || {_distanceSqrd:Number.MAX_VALUE};
            idxStart = idxStart || 0;
            idxEnd   = idxEnd || points.length;
            for (var i = idxStart+1; i < idxEnd; i++) {
                var segment = new geometry.PixLine(points[i-1], points[i]);
                var projection = segment.projectOnto(pixPoint, true);
                var distance   = new geometry.PixLine(projection, pixPoint).lengthSqrd();
                if (distance < nearestSegment._distanceSqrd) {
                    nearestSegment = {
                        pix : {
                            projection : projection,
                            segment    : segment
                        },
                        idxStart     : i-1,
                        idxEnd       : i,
                        _distanceSqrd : distance
                    };
                }
            }
            nearestSegment.pix.distance = new geometry.PixLine(nearestSegment.pix.projection, pixPoint).length();
            return nearestSegment;
        }

        /*
         * _originalPoints is an array of projected geographical points (screen coordinates of the polygon points),
         * and _parts is an array of arrays of points that eventually gets rendered after clipping and simplifying points.
         */
        var originalPoints = this.mDrawnRoute._originalPoints;
        var parts          = this.mDrawnRoute._parts;
        utils.assert(this.mDrawnRoute._latlngs.length == originalPoints.length);
        var nearestSegment = undefined;
        for (var j = 0; j < parts.length; j++) {
            nearestSegment = projectOnto(parts[j], nearestSegment);
        }

        //nearestSegment.projection is sought after projection onto 'this', but it's been found scanning _parts
        //and thus the underlying subsegment needs to be found with originalPoints between nearestSegment.pixStart
        //and nearestSegment.pixEnd
        if (nearestSegment) {
            var startIdx;
            for (var i = 0; i < originalPoints.length; i++) {
                if (nearestSegment.pix.segment.mStart.equals(originalPoints[i])) {
                    startIdx = i;
                }
                if (nearestSegment.pix.segment.mEnd.equals(originalPoints[i])) {
                    nearestSegment = projectOnto(originalPoints, undefined, startIdx, i+1);
                    break;
                }
            }
            var ratio = new geometry.PixLine(
                nearestSegment.pix.segment.mStart, nearestSegment.pix.projection
            ).length()/nearestSegment.pix.segment.length();
            nearestSegment.start = this.mWaypoints[nearestSegment.idxStart];
            nearestSegment.end   = this.mWaypoints[nearestSegment.idxEnd];
            nearestSegment.segment = new geometry.GeoLine(nearestSegment.start,nearestSegment.end);
            nearestSegment.projection = nearestSegment.segment.offsetRatio(ratio);
        }
        return nearestSegment;
    };

    Section.prototype.projectLatLngOnto = function(latlng)
    {
        var nearestSegment = undefined;
        var minDistance = Number.MAX_VALUE;
        for (var i = 1; i < this.mWaypoints.length; i++) {
            var segment = new geometry.GeoLine(
                this.mWaypoints[i-1],
                this.mWaypoints[i]
            );
            var projection = segment.projectOnto(latlng, true);
            var distance   = latlng.distanceTo(projection);
            if (minDistance > distance) {
                minDistance = distance;
                nearestSegment = {
                    idxStart   : i-1,
                    idxEnd     : i,
                    start      : this.mWaypoints[i-1],
                    end        : this.mWaypoints[i],
                    segment    : segment,
                    projection : projection,
                };
            }
        }
        return nearestSegment;
    };

    Section.prototype.draw = function()
    {
        function mouseMove(e) {
            if (lastHoverEvent.layerPoint.distanceTo(e.layerPoint) > KPixTolerance) {
                if (tempMarker) {
                    if (!tempMarker.mBeingDragged) {
                        tempMarker.delete();
                    }
                    tempMarker = undefined;
                }
                map.leaflet.off("mousemove", mouseMove);
                self.mParent.highlight(false);
            }
        };
        var self = this;
        if (this.mDrawnRoute) {
            map.leaflet.removeLayer(this.mDrawnRoute);
        }
        if (this.mWaypoints) {
            this.mDrawnRoute = new L.Polyline(this.mWaypoints, KRouteStyle).addTo(map.leaflet);

            var tempMarker          = undefined;
            var lastHoverEvent      = undefined;
            var KPixTolerance       = 10;
            this.mDrawnRoute.on('mouseover', function(e) {
                var projection = self.projectPixOnto(e.layerPoint);
                self.mParent.highlight(
                    projection.start.decoration.distanceFromStartMts
                );
                lastHoverEvent = e;
                if (tempMarker) {
                    tempMarker.setLatLng(projection.projection, true);
                } else {
                    tempMarker = new Section(self.mParent, {
                        prev     : self,
                        next     : self.mNext,
                        opacity  : 0.5,
                        iconClass: 'toFromIcon',
                        latlng   : projection.projection
                    });
                    map.leaflet.on("mousemove", mouseMove);
                }
            });
            this.mDrawnRoute.bringToFront();
        }
        this.unstale();
    };

    Section.prototype.delete = function() {
        this.clear();
        if (!this.isTemp()) {
            if (this.mNext) {
                this.mNext.mPrev = this.mPrev;
            }
            if (this.mPrev == undefined) {
                this.mParent.head(this.mNext);
            } else {
                this.mPrev.clear();
                this.mPrev.mNext = this.mNext;
            }
            this.mParent.invalidate();
        }

        WayMarkerBase.prototype.delete.call(this);
    };

    Section.prototype.clear = function() {
        this.mWaypoints = undefined;
        if (this.mDrawnRoute) {
            map.leaflet.removeLayer(this.mDrawnRoute);
        }
    }

    Section.prototype.getBounds = function() {
        if (this.mDrawnRoute) {
            return this.mDrawnRoute.getBounds();
        }
    }

    Section.prototype.highlight = function(highlight) {
        if (this.mDrawnRoute) {
            if (highlight) {
                this.mDrawnRoute.setStyle(KRouteStyleHighlight);
            } else {
                this.mDrawnRoute.setStyle(KRouteStyle);
            }
        }
    };









    function WaypointIterator(section, wayPointIndex) {
        this.mSection = section;
        if (wayPointIndex) {
            this.mWayPointIdx = wayPointIndex;
        }
    }

    WaypointIterator.prototype.current = function() {
        if (!("mSection" in this)) {
            return undefined;
        }
        if ("mWayPointIdx" in this && this.mSection.mWaypoints) {
            return this.mSection.mWaypoints[this.mWayPointIdx];
        }
        return this.mSection.getLatLng();
    }

    WaypointIterator.prototype.next = function(offset) {
        var self = this;
        function next() {
            if (self.mSection.isTemp()) {
                self.mWayPointIdx = self.mSection.mPrev.projectLatLngOnto(
                    self.mSection.getLatLng()
                ).idxStart;
                self.mSection     = self.mSection.mPrev;
            }
            if ("mWayPointIdx" in self) {
                if (self.mWayPointIdx == self.mSection.mWaypoints.length - 1) {
                    //last point of the section
                    if (self.mSection.mNext && self.mSection.mNext.mWaypoints) {
                        self.mSection = self.mSection.mNext;
                        delete self["mWayPointIdx"];
                    } else {
                        //fell off the cliff, iterator invalid now
                        delete self["mWayPointIdx"];
                        delete self["mSection"];
                        return undefined;
                    }
                } else {
                    ++self.mWayPointIdx;
                }
            } else if (self.mSection.mWaypoints) {
                self.mWayPointIdx = 0;
            } else {
                delete self["mWayPointIdx"];
                delete self["mSection"];
                return undefined;
            }
            return self.current();
        }

        offset = offset || 1;
        var metres = 0;
        var projectionTolerance = undefined;
        if (typeof offset === 'object') {
            if (offset.metres) {
                metres = offset.metres;
                projectionTolerance = 40;
            } else if (offset.minMetres) {
                metres = offset.minMetres;
            } else {
                console.error("Wrong offset!!");
                console.dir(offset);
            }
            offset = 1;
        }
        var previous = undefined;
        var result   = this.current();
        for (;offset > 0 || metres > 0; offset--) {
            previous = result;
            result   = next();
            if (!result) {
                return undefined;
            }
            metres -= previous.distanceTo(result);
        }
        metres = Math.abs(metres);
        if (projectionTolerance && (metres > projectionTolerance)) {
            //asked for some metres (not minMetres), but we've now overshot and are too far behind the current result
            if (result.distanceTo(previous) < projectionTolerance) {
                //we've overshot by a smaller distance than projectionTolerance, so return previous
                result = previous;
            } else {
                //project
                result = new geometry.GeoLine(previous, result).offsetMts(metres);
            }
        }
        return result;
    }

    WaypointIterator.prototype.peekNext = function(offset) {
        return new WaypointIterator(this.mSection, this.mWayPointIdx).next(offset);
    }

    return {
        Route           : Route,
        WayMarkerBase   : WayMarkerBase,
        Section         : Section
    }

})();

module.exports = route;