/**
 * scripts/ui/ui.js
 *
 * This prepares and manages the Zikes's UI.
 */

'use strict';

var journeys = require("./journeys.js")
var ui = (function() {

    return {
        map      : require("./map.js"),
        journeys : new journeys.Journeys($("#journeys"), $("#journey"))
    }

})();

module.exports  = ui;