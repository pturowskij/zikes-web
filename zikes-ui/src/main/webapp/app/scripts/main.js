/**
 * scripts/main.js
 *
 * This is the starting point for your application.
 * Take a look at http://browserify.org/ for more info
 */

'use strict';

global.zikes = {
	ui: require('./ui/ui.js') //this is to allow html refer to some entry points
};

