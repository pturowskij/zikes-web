/**
 * scripts/lib/utils.js
 * Library of utils
 */

'use strict';

var utils = (function() {

    return {
	    mtsToString : function(mts) {
	        var result = "0mts"
	        if (mts) {
	            if (mts < 1000) {
	                result = mts+"mts";
	            } else if (mts < 5000) {
	                result = (mts/1000).toFixed(1);
	                result += "km";
	            } else {
	                result = ((mts/1000) | 0) +"km";
	            }
	        }
	        return result;
	    },
	    millisToString : function(millis) {
	        var result = "0ms"
	        if (millis) {
	            if (millis > 1000) {
	                result = (millis/1000).toFixed(2) + "s";
	            } else if (millis < 10) {
	                result = millis.toFixed(2);
	                result += "ms";
	            } else {
	                result = (millis|0) +"ms";
	            }
	        }
	        return result;
	    },
	    largeNumberToStr: function(number) {
	        var result = "0"
	        var suffixes = ["", "k", "m"];
	        for (var i = 0; i < suffixes.length; i++) {
	            if (number < 999) {
	                break;
	            }
	            number = (number / 1000).toFixed(2);
	        }
	        if (number > 10) {
	            number = number | 0;
	        }
	        return number + suffixes[i];
    	},
 		assert: function(condition, message) {
		    if (!condition) {
		        message = "Assertion failed: '"+message+"'";
		        console.error(message);
		        if (window.location.hostname == "localhost") {
		            throw new Error(message);
		        }
		    }
		}
	}

})();

module.exports = utils;