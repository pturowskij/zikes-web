/**
 * scripts/lib/geoip.js
 *
 * This works out where the browser is (based on ip address)
 */

'use strict';

var when  = require("when");
var http  = require("./http.js");

var geoip = (function() {

    function whatsMyIp() {
        return http.get("http://api.ipify.org/?format=json")
        .then(function(response) {
            return response.ip;
        });
    }

    function geoipFind(ip) {
        return http.get("http://freegeoip.net/json/"+ip)
        .then(function(response) {
            return {
                latlng : new L.LatLng(response.latitude, response.longitude),
                country: response.country_name
            }
        });
    }

    function get() {
        return when(whatsMyIp()).then(geoipFind);
    }

    return {
        get  : get
    };
})();

module.exports = geoip;