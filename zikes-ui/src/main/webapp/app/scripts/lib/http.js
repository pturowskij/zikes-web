/**
 * scripts/lib/http.js
 *
 * This is a promise based http client
 */

'use strict';

var when     = require("when");

var http = (function() {

    function HttpRequest(url) {
        this.mUrl = url;
        this.mXmlHttp = new XMLHttpRequest();
        this.mHeaders = {};
    }

    HttpRequest.prototype.headers = function(headers) {
        for (var headerName in headers) {
            this.mHeaders[headerName] = headers[headerName]
        }
        return this;
    }

    function send(self, method, data) {
        var result = when.defer();
        console.log("http."+method.toLowerCase()+"("+self.mUrl+") >>");

        self.mXmlHttp.onreadystatechange = function() {
            if (self.mXmlHttp.readyState == 4) {
                var response = self.mXmlHttp.response;
                try {
                    response = JSON.parse(response);
                } catch (err) {}
                if (self.mXmlHttp.status == 200 || self.mXmlHttp.status == 201) {
                    console.log("http."+method.toLowerCase()+"("+self.mUrl+") <<");
                    result.resolve(response);
                } else {
                    console.error("http."+method.toLowerCase()+"("+self.mUrl+") << "+self.mXmlHttp.response);
                    result.reject(response);
                }
            }
        };
        self.mXmlHttp.open(method.toUpperCase(), self.mUrl, true );
        if (data && typeof data === "object") {
            self.mXmlHttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            data = JSON.stringify(data);
        }
        for (var headerName in self.mHeaders) {
            self.mXmlHttp.setRequestHeader(headerName, self.mHeaders[headerName]);
        }
        self.mXmlHttp.send(data);

        return result.promise;
    }

    HttpRequest.prototype.get = function() {
        return send(this, "get");
    }

    HttpRequest.prototype.delete = function() {
        return send(this, "delete");
    }

    HttpRequest.prototype.post = function(data) {
        return send(this, "post", data);
    }

    HttpRequest.prototype.put = function(data) {
        return send(this, "put", data);
    }

    function get(url) {
        return new HttpRequest(url).get();
    }

    function post(url, jsonData) {
        return new HttpRequest(url).post(jsonData);
    }

    return {
        get         : get,
        post        : post,
        HttpRequest : HttpRequest
    };
})();

module.exports = http;