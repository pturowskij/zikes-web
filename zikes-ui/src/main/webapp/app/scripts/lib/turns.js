/**
 * scripts/lib/popularity.js
 *
 * This, based on the value of the master slider,
 * calculates and returns the turn preference
 */

'use strict';

var turnPreference = (function() {


    function TurnSliderPreference(parent, jsonDocument) {
        this.mParent   = parent;
        this.mJSON     = jsonDocument;
    }

    var maxValue = 50;
    TurnSliderPreference.prototype.masterSlideOptions = function() {
        var result = {
            label: "turns",
            value: maxValue,
            min: {
                value: 0,
                label: "less turns"
            },
            max: {
                value: maxValue,
                label: ""
            },
            step: 2
        }
        this.mMasterSliderValue = 0;
        return result;
    }


    TurnSliderPreference.prototype.fromMasterSlider = function(masterSliderValue, rootJson) {
        this.mMasterSliderValue = maxValue-masterSliderValue
        if (rootJson) {
            return this.generate(rootJson);
        }
    }

    TurnSliderPreference.prototype.render = function(panel) {
    }

    TurnSliderPreference.prototype.generate = function(rootJson) {
        if (this.mMasterSliderValue) {
            rootJson["turnPunishmentsMts"] = {
                easy : this.mMasterSliderValue,
                difficult : {

                }
            }
            var highwayFactors = rootJson["highwayCostFactors"]
            for (var highwayType in highwayFactors) {
                var factors4HighwayType = highwayFactors[highwayType];
                rootJson["turnPunishmentsMts"]["difficult"][highwayType] = (parseInt(factors4HighwayType["cost"]) * this.mMasterSliderValue) | 0;
            }
        }
    }

    return {
        TurnSliderPreference: TurnSliderPreference,
        sliderGenerator : TurnSliderPreference
    }

})();

module.exports = turnPreference;