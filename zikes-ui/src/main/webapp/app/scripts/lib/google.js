/**
 * scripts/lib/google.js
 *
 * This proxies the google map api
 */

'use strict';

var when = require("when");
var http = require("./http.js");

var googleAPI = (function() {

    var apikey="AIzaSyA58faUIH8LUCRpS0smLvHYKNBk_a7TwZs";
    var addressComponentsSpecificity = [
        {//0
            name    : "country",
            priority:   1
        },
        {//1
            name: "administrative_area_level_1",
        },
        {//2
            name: "administrative_area_level_2"
        },
        {//3
            name: "locality",
            priority:   1
        },
        {//4
            name: "sublocality"
        },
        {//5
            name: "neighborhood",
            priority:   1
        },
        {//6
            name: "route",
            priority:   1
        },
        {//7
            name: "street_number"
        }
    ];

    function geocodeFind(query) {
        query = query.replace(" ", "+");
        return http.get("https://maps.googleapis.com/maps/api/geocode/json?address="+query+"&key="+apikey)
        .then(function(response) {
            if (response.status != "OK") {
                console.error(response.error_message);
                return when.reject(response.error_message);
            }
            return response.results;
        });
    }


    function reverseGeocodeFind(latlng) {
        if (!latlng) {
            return when(undefined);
        }
        return http.get("https://maps.googleapis.com/maps/api/geocode/json?latlng="+latlng[0]+","+latlng[1]+"&key="+apikey)
        .then(function(response) {
            if (response.status != "OK") {
                console.error(response.error_message);
                return when.reject(response.error_message);
            }
            var components = [];
            for (var i = 0; i < addressComponentsSpecificity.length; i++) {
                var componentType = addressComponentsSpecificity[i];
                components.push(findComponent(response.results, componentType.name));
            }
            return components;
        });
    }


    function findComponent(revGeoCodeResults, componentType) {
        for (var j = 0; j < revGeoCodeResults.length ; j++) {
            var addressComponents = revGeoCodeResults[j].address_components;
            for (var i = 0; i < addressComponents.length; i++) {
                var component = addressComponents[i];
                for (var t = 0; t < component.types.length; t++) {
                    if (component.types[t] == componentType) {
                        return component.short_name;
                    }
                }
            }
        }
    }


    //Pass results of one or two different calls to reverseGeocodeFind
    //and obtain (respectivelly) one or two street addresses truncated to the delta between
    //them (cutting away the common part)
    function geocodeHumanReadable(geocodeResults, verbosity) {
        verbosity = verbosity || 2;
        //deep copy first:
        var unpack = false;
        if (!Array.isArray(geocodeResults[0])) {
            geocodeResults =  [geocodeResults];
            unpack = true;
        }

        function compress() {
            var compressed = [];
            var lastPresentComponentIdx = undefined;
            for (var i = 0; i < addressComponentsSpecificity.length; i++) {
                var prev = undefined;
                var current = undefined;
                for (var j = 0; j < geocodeResults.length; j++) {
                    current = geocodeResults[j][i];
                    if (current == undefined) {
                        break;
                    }
                    if (prev || geocodeResults.length == 1) {
                        if (prev == current && compressed.length == 0 && i < 6) {
                            break;
                        } else {
                            if (compressed.length == 0 && lastPresentComponentIdx) {
                                compressed.push({index:lastPresentComponentIdx, priority:-1});
                            }
                            compressed.push({index:i});
                        }
                    }
                    prev = current;
                }
                if (current) {
                    lastPresentComponentIdx = i;
                }

            }
            return compressed.map(function(delta) {
                delta.priority = delta.priority || addressComponentsSpecificity[delta.index].priority || 0;
                delta.delta    = [];
                for (var j = 0; j < geocodeResults.length; j++) {
                    delta.delta.push(geocodeResults[j][delta.index]);
                }
                return delta;
            });
        }
        var compressed = compress();
        var resultIndeces = [];
        for (var p = 7; p >= -1 && resultIndeces.length < verbosity; p--) {
            for (var i = 0; i < compressed.length && resultIndeces.length < verbosity; i++) {
                if (compressed[i].priority == p) {
                    resultIndeces.push(i);
                }
            }
        }
        resultIndeces = resultIndeces.sort();
        var result = geocodeResults.map(function(){return [];});
        for (var i = 0; i < resultIndeces.length;i++) {
            var delta = compressed[resultIndeces[i]].delta;
            for (var j = 0; j < delta.length; j++) {
                result[j].push(delta[j]);
            }
        }
        result = result.map(function(elem) {
            return elem.join(", ");
        });
        if (unpack) {
            result = result[0];
        }
        return result;
    }

    return {
        geocodeFind             : geocodeFind,
        reverseGeocodeFind      : reverseGeocodeFind,
        geocodeHumanReadable    : geocodeHumanReadable
    };

})();

module.exports = googleAPI;''