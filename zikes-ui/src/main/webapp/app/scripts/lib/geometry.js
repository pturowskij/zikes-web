/**
 * scripts/lib/geometry.js
 */

'use strict';

var geometry = (function() {

    function dot(v1,v2) {
        return (v1.x*v2.x)+(v1.y*v2.y);
    }

    function ldot(v1,v2) {
        return (v1.lat*v2.lat)+(v1.lng*v2.lng);
    }

    var GeoVector = L.LatLng;
    function GeoLine(start, end) {
        this.mStart = start;
        this.mEnd   = end;
        this.mV     = new GeoVector(this.mEnd.lat - this.mStart.lat, this.mEnd.lng - this.mStart.lng);
    }

    GeoLine.prototype.projectOnto = function(geoPoint, force) {
        if (this.lengthSqrd() == 0) {
            //0-lenght segment => point
            return force ? this.mStart : undefined;
        }
        var projectionRatio = ldot(
            this.mV,
            new GeoVector(geoPoint.lat - this.mStart.lat, geoPoint.lng - this.mStart.lng) //GeoVector: mStart->geoPoint
        )/this.lengthSqrd();

        if (projectionRatio < 0) {
            //beyond the mEnd end of the segment
            return force ? this.mEnd : undefined;
        } else if (projectionRatio > 1) {
            //beyond the mStart end of the segment
            return force ? this.mStart : undefined;
        }
        return new L.LatLng(
            this.mStart.lat + (projectionRatio * this.mV.lat),
            this.mStart.lng + (projectionRatio * this.mV.lng)
        );
    }

    GeoLine.prototype.offsetMts = function(metresFromStart) {
        var segmentLengthMts   = this.mStart.distanceTo(this.mEnd);
        return this.offsetRatio(metresFromStart/segmentLengthMts);
    }

    GeoLine.prototype.offsetRatio = function(ratio) {
        return new L.LatLng(
            this.mStart.lat + (this.mV.lat * ratio),
            this.mStart.lng + (this.mV.lng * ratio)
        );
    }

    GeoLine.prototype.lengthSqrd = function() {
        return Math.pow(this.mV.lat,2)+Math.pow(this.mV.lng,2);
    }

    GeoLine.prototype.length = function() {
        return Math.sqrt(this.lengthSqrd());
    }




    var PixVector = L.Point;
    function PixLine(start, end) {
        this.mStart = start;
        this.mEnd   = end;
        this.mV     = new PixVector(this.mEnd.x - this.mStart.x, this.mEnd.y - this.mStart.y);
    }

    PixLine.prototype.projectOnto = function(pixPoint, force) {
        if (this.lengthSqrd() == 0) {
            //0-lenght segment => point
            return force ? this.mStart : undefined;
        }
        var projectionRatio = dot(
            this.mV,
            new PixVector(pixPoint.x - this.mStart.x, pixPoint.y - this.mStart.y) //PixVector: mStart->pixPoint
        )/this.lengthSqrd();

        if (projectionRatio < 0) {
            //beyond the mEnd end of the segment
            return force ? this.mEnd : undefined;
        } else if (projectionRatio > 1) {
            //beyond the mStart end of the segment
            return force ? this.mStart : undefined;
        }
        return new L.Point(
            this.mStart.x + (projectionRatio * this.mV.x),
            this.mStart.y + (projectionRatio * this.mV.y)
        );
    }

    PixLine.prototype.offsetMts = function(metresFromStart) {
        var segmentLengthMts   = this.mStart.distanceTo(this.mEnd);
        return this.offsetRatio(metresFromStart/segmentLengthMts);
    }

    PixLine.prototype.offsetRatio = function(ratio) {
        return new L.LatLng(
            this.mStart.x + (this.mV.x * ratio),
            this.mStart.y + (this.mV.y * ratio)
        );
    }

    PixLine.prototype.lengthSqrd = function() {
        return Math.pow(this.mV.x,2)+Math.pow(this.mV.y,2);
    }

    PixLine.prototype.length = function() {
        return Math.sqrt(this.lengthSqrd());
    }



    return {
        GeoLine : GeoLine,
        PixLine : PixLine
    }

})();

module.exports = geometry;