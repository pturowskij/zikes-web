/**
 * scripts/lib/zikes.js
 *
 * This handles communication with the zikes-api, i.e.: the backend.
 */

'use strict';

var http = require("./http.js");
var auth = require("../ui/auth.js");
var when = require("when");

var zikesRouter = (function() {


    /****************************************************************************
     *
     *
     * Communication with the routing server
     *
     *
     ****************************************************************************/
    var pendingRequest = undefined;
    var zikesServer = document.location.hostname != "localhost" ? "https://api-dot-zikes-web-client.appspot.com" : "http://localhost:7777";
    function requestRoute(coordinates, preferences) {
        if (pendingRequest) {
            return when.reject(new Error("busy"));
        }
        var coordinatesStr = "";
        for (var i = 0; i < coordinates.length; i++) {
            var milestone = coordinates[i];
            coordinatesStr+=milestone[0]+","+milestone[1];
            if (i+1 < coordinates.length) {
                coordinatesStr += ",";
            }
        }

        return http.post(zikesServer + "/route?milestones="+coordinatesStr, preferences)
        .otherwise(function(reason) {
            if (reason.message) {
                reason = reason.message;
                try {
                    reason = JSON.parse(reason);
                } catch(err) {}
            }
            var error = new Error(err);
            error.reason = reason;
            throw error;
        });
    }

    function meta() {
        return http.get(zikesServer + "/meta.json");
    }



    /****************************************************************************
     *
     *
     * Authentication with zikes-api function for the journey/user meta
     *
     *
     ****************************************************************************/
    var authProviderHeaderName = "Authorization-Provider";
    var authTokenHeaderName    = "Authorization";

    function authHeaders(userData) {
        var result = {};
        result[authProviderHeaderName] = userData.network.toUpperCase();
        result[authTokenHeaderName]    = "Bearer "+userData.authResponse.access_token; //Bearer see: http://self-issued.info/docs/draft-ietf-oauth-v2-bearer.html
        return result;
    }

    var userData = when.defer();
    auth.on("logout", function() {
        userData = when.defer();
    });
    auth.on("login", function(ud) {
        userData.resolve(login());
    });

    function login() {
        return auth.login("Login to complete action")
        .then(function(userData) {
            if (!userData.zikesUserMetaData) {
                return new http.HttpRequest(
                    zikesServer + "/users/metadata"
                ).headers(
                    authHeaders(userData)
                ).get()
                .then(function(zikesUserMetaData) {
                    userData.zikesUserMetaData = zikesUserMetaData;
                    return userData;
                });
            }
            return userData;
        });
    }

    function isLoggedIn() {
        userData.promise.inspect().state == "fulfilled";
    }


    /****************************************************************************
     *
     *
     * Planned journeys and preferences
     *
     *
     ****************************************************************************/
    function userPath(userData) {
        return zikesServer + "/users/"+userData.zikesUserMetaData.encodedId;
    }

    function userPlanPath(userData) {
        return userPath(userData)+"/plan";
    }

    function userPlanJourneysPath(userData) {
        return userPlanPath(userData)+"/journeys"
    }

    function plannedJourneys() {
        return login().then(function(userData) {
            return new http.HttpRequest(
                userPlanJourneysPath(userData)
            ).headers(
                authHeaders(userData)
            ).get();
        });
    }

    function loadPlannedJourney(id) {
        return login().then(function(userData) {
            return new http.HttpRequest(
                userPlanJourneysPath(userData)+"/"+id
            ).headers(
                authHeaders(userData)
            ).get();
        });
    }

    function deletePlannedJourney(id) {
        return login().then(function(userData) {
            return new http.HttpRequest(
                userPlanJourneysPath(userData)+"/"+id
            ).headers(
                authHeaders(userData)
            ).delete();
        });
    }

    function savePlannedJourney(journey) {
        return login().then(function(userData) {
            var req = new http.HttpRequest(
                userPlanJourneysPath(userData)+"/"+(journey.metadata.id || "")
            ).headers(
                authHeaders(userData)
            );
            if (journey.metadata.id) {
                //update
                req = req.put(journey);
            } else {
                //create
                req = req.post(journey);
            }
            return req.then(function(response) {
                journey.metadata = response;
                return journey.metadata;
            });
        });
    }

    function userPlanPreferencesPath(userData) {
        return userPlanPath(userData)+"/preferences"
    }

    function preferences(values) {
        return login().then(function(userData) {
            var globalPreferencesUrl = userPlanPreferencesPath(userData)+"/"+0;
            if (values) {
                //setter
                return new http.HttpRequest(
                    globalPreferencesUrl
                ).headers(
                    authHeaders(userData)
                ).put({
                    metadata: {
                        access: {
                            ro: "",
                            rw: ""
                        }
                    },
                    preferences: values
                });
            } else {
                //getter
                return new http.HttpRequest(
                    globalPreferencesUrl
                ).headers(
                    authHeaders(userData)
                ).get().then(function(preferneces) {
                    return preferneces.preferneces
                }).otherwise(function(err) {
                    //preferneces may not be set yet, ignore 404
                    if (err.status == 404) {
                        return {
                            lastProfile: "vanilla bicycle"
                        };
                    }
                    throw err;
                });
            }
        });
    }

    function profiles(forceAuth) {
        if (forceAuth) {
            login();
        }
        return when(userData.promise).then(function(userData) {
            return new http.HttpRequest(
                userPlanPreferencesPath(userData)
            ).headers(
                authHeaders(userData)
            ).get();
        });
    }

    function deleteProfile(id) {
        return login().then(function(userData) {
            return new http.HttpRequest(
                userPlanPreferencesPath(userData)+"/"+id
            ).headers(
                authHeaders(userData)
            ).delete();
        });
    }


    function saveProfile(profile) {
        return login().then(function(userData) {
            var req = new http.HttpRequest(
                userPlanPreferencesPath(userData)+"/"+(profile.metadata.id || "")
            ).headers(
                authHeaders(userData)
            );
            if (profile.metadata.id) {
                //update
                req = req.put(profile);
            } else {
                //create
                req = req.post(profile);
            }
            return req.then(function(response) {
                profile.metadata = response;
                return profile.metadata;
            });
        });
    }



    function publicProfiles() {
        return when({
            "vanilla bicycle" : {
                sliders: {
                    roads:{
                        masterSlider:[0,110],
                        generator:{path:{value:0.9090909090909091},track:{value:1},steps:{value:0.6363636363636364},pedestrian:{value:1},cycleway:{value:1},residential:{value:1},road:{value:1},secondary:{value:1},primary:{value:1},trunk:{value:1},motorway:{value:1}}
                    },
                    climb:{masterSlider:20},
                    turns:{masterSlider:50},
                    cycleways:{masterSlider:1},
                    urbanrural:{masterSlider:20},
                    popularity:{masterSlider:1}
                },
                means: "meansBicycle"
            }
        });
    }

    return {
        requestRoute         : requestRoute,
        meta                 : meta,
        preferences          : preferences,
        profiles             : profiles,
        publicProfiles       : publicProfiles,
        saveProfile          : saveProfile,
        deleteProfile        : deleteProfile,
        plannedJourneys      : plannedJourneys,
        loadPlannedJourney   : loadPlannedJourney,
        savePlannedJourney   : savePlannedJourney,
        deletePlannedJourney : deletePlannedJourney,
        isLoggedIn           : isLoggedIn
    };
})();

module.exports = zikesRouter;