/**
 * scripts/lib/climb.js
 *
 * This, based on the value of the master slider,
 * calculates and returns the climb preference
 */

'use strict';

var climbPreference = (function() {

    var KSliderMax                = 40;
    var KMinNeutralDescentPercent = 8;
    var KMinMaxClimbPercent       = 20;
    var KIgnoreToClimbPercent     = 2;


    function sqrtGenerator(sliderValue, climbPercent) {
        sliderValue = 1.0*sliderValue/4;
        return sliderValue * Math.sqrt(climbPercent);
    }

    //this generates a nice distribution that starts with 0
    //maximasies at 8, reaches 0 again around 14 and goes
    //below zero thereafter
    function sinGenerator(steps) {
        var result = [];
        for (var step = 0; step < steps; step++) {
            var x = step*(20/steps);
            result.push(
                Math.sin(2.0*x/(Math.PI*3.0))
            )
        }
        return result;
    }

    function ClimbSliderPreference(parent, jsonDocument) {
        this.mParent   = parent;
        this.mJSON     = jsonDocument;
    }



    ClimbSliderPreference.prototype.masterSlideOptions = function() {
        var result = {
            label: "climb",
            value: KSliderMax/2,
            min: {
                value: 0,
                label: "flat"
            },
            max: {
                value: KSliderMax,
                label: "hills"
            },
            step: 1
        }
        this.mMasterSliderValue = 0;
        return result;
    }


    ClimbSliderPreference.prototype.fromMasterSlider = function(masterSliderValue, rootJson) {
        this.mMasterSliderValue = masterSliderValue - KSliderMax/2;
        if (rootJson) {
            return this.generate(rootJson);
        }
    }

    ClimbSliderPreference.prototype.render = function(panel) {
    }



    ClimbSliderPreference.prototype.generate = function(rootJson) {

        if (this.mMasterSliderValue == 0) {
            return;
        }

        function generateHalfClimb(cb) {
            for (var i = 1; i <= KMinMaxClimbPercent-KIgnoreToClimbPercent; i++) {
                cb(i);
            }
        }

        function fillFront(a) {
            return new Array(KMinMaxClimbPercent-a.length).fill(1.0).concat(a);
        }

        var self        = this;
        var halfClimb   = [];
        var halfDescend = [];
        if (this.mMasterSliderValue < 0) {
            generateHalfClimb(function(climbPercent) {
                halfClimb.push(
                    sqrtGenerator(-self.mMasterSliderValue, climbPercent)
                );
            });
            halfDescend = halfClimb.slice(0,KMinMaxClimbPercent-KMinNeutralDescentPercent);
        } else {
            halfClimb = sinGenerator(
                KMinMaxClimbPercent-KIgnoreToClimbPercent
            );
            var sliderValue = self.mMasterSliderValue/7;
            halfClimb = halfClimb.map(function(elem) {
                if (elem < 0) {
                    elem = 1+Math.abs(elem)*sliderValue
                } else if (elem > 0) {
                    elem = 1.0/(1+elem*sliderValue);
                } else {
                    elem = 1.0;
                }
                return elem;
            });
            halfDescend = halfClimb.slice(0);
        }

        var result = fillFront(halfDescend)
        .reverse()
        .concat([1.0])
        .concat(fillFront(halfClimb))
        .map(function(elem) { return elem.toFixed(2);})
        rootJson["climbCostFactors"] = {
            costFactors : result
        }

    }

    return {
        ClimbSliderPreference: ClimbSliderPreference,
        sliderGenerator : ClimbSliderPreference
    }

})();

module.exports = climbPreference;