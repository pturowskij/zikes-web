/**
 * scripts/lib/cycleway.js
 *
 * This, based on the value of the master slider,
 * calculates and returns the cycleway preference
 */

'use strict';

var cyclewayPreference = (function() {


    function CyclewaySliderPreference(parent, jsonDocument) {
        this.mParent   = parent;
        this.mJSON     = jsonDocument;
    }

    CyclewaySliderPreference.prototype.masterSlideOptions = function() {
        var result = {
            label: "cycleways",
            value: 1,
            min: {
                value: 1,
                label: ""
            },
            max: {
                value: 0.3,
                label: "more cycleways"
            },
            step: -0.05
        }
        this.mMasterSliderValue = result.value;
        return result;
    }


    CyclewaySliderPreference.prototype.fromMasterSlider = function(masterSliderValue, rootJson) {
        this.mMasterSliderValue = masterSliderValue
        if (rootJson) {
            return this.generate(rootJson);
        }
    }

    CyclewaySliderPreference.prototype.render = function(panel) {
    }

    CyclewaySliderPreference.prototype.generate = function(rootJson) {
        var highwayFactors = rootJson["highwayCostFactors"]
        highwayFactors["default"] = {
            designated: this.mMasterSliderValue.toFixed(2)
        }
        for (var highwayType in highwayFactors) {
            var factors4HighwayType = highwayFactors[highwayType];
            factors4HighwayType["designated"] = (parseInt(factors4HighwayType["cost"]) * this.mMasterSliderValue).toFixed(2);
        }
    }

    return {
        CyclewaySliderPreference: CyclewaySliderPreference,
        sliderGenerator : CyclewaySliderPreference
    }

})();

module.exports = cyclewayPreference;